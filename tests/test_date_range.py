import datetime
from datetime import timedelta

import pytest

from mfe_ms.date_range import DateRange


def test_init_ok(today):
    DateRange(start=today, end=today)
    DateRange(start=today, end=today + timedelta(days=1))


def test_init_raises_value_error(today):
    with pytest.raises(ValueError):
        DateRange(start=today, end=today - timedelta(days=1))


@pytest.mark.parametrize(
    ("start",),
    (
        (timedelta(days=-1),),
        (datetime.datetime.now(),),
        (12,),
        ("2012-12-12",),
    ),
)
def test_init_start_raises_type_error(today, start):
    with pytest.raises(TypeError):
        DateRange(start=start, end=today)


@pytest.mark.parametrize(
    ("end",),
    (
        (timedelta(days=-1),),
        (datetime.datetime.now(),),
        (12,),
        ("2012-12-12",),
    ),
)
def test_init_end_raises_type_error(today, end):
    with pytest.raises(TypeError):
        DateRange(start=today, end=end)


@pytest.mark.parametrize(
    ("a", "b", "expected"),
    (
        (
            DateRange(start=datetime.date(2000, 1, 1), end=datetime.date(2000, 1, 1)),
            DateRange(start=datetime.date(2000, 1, 1), end=datetime.date(2000, 1, 1)),
            DateRange(start=datetime.date(2000, 1, 1), end=datetime.date(2000, 1, 1)),
        ),
        (
            DateRange(start=datetime.date(2000, 1, 1), end=datetime.date(2000, 10, 1)),
            DateRange(start=datetime.date(2000, 1, 3), end=datetime.date(2000, 7, 1)),
            DateRange(start=datetime.date(2000, 1, 3), end=datetime.date(2000, 7, 1)),
        ),
        (
            DateRange(start=datetime.date(2000, 1, 1), end=datetime.date(2000, 1, 1)),
            DateRange(start=datetime.date(1000, 1, 1), end=datetime.date(1000, 1, 1)),
            None,
        ),
        (
            DateRange(start=datetime.date(2000, 1, 1), end=datetime.date(4000, 1, 1)),
            DateRange(start=datetime.date(1000, 1, 1), end=datetime.date(3000, 1, 1)),
            DateRange(start=datetime.date(2000, 1, 1), end=datetime.date(3000, 1, 1)),
        ),
    ),
)
def test_intersection(a, b, expected):
    assert a.intersection(b) == expected
    assert b.intersection(a) == expected
    assert a.intersection(a) == a
    assert b.intersection(b) == b
    if expected is None:
        assert not a.intersects(b)
        assert not b.intersects(a)
    else:
        assert a.intersects(b)
        assert b.intersects(a)
