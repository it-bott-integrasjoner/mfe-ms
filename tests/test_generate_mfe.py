import logging
from random import randrange
from datetime import date, timedelta
from types import SimpleNamespace

import pytest
from dfo_sap_client.models import Ansatt

from mfe_ms.date_range import DateRange
from mfe_ms.generate_mfe import (
    Entry,
    generate_data,
    process_data,
    EmployeeMapper,
    logger as generate_mfe_logger,
    Termintype,
    _extract_latest_terminovervakning,
    _make_terminovervakning_dict,
    NoAccess,
)


def test_process_data(mock_cache, mantall, test_config, date_range):
    """Check that test data produces correct output"""
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employees = mock_cache.get_employees()
    orgs = {i.id: i for i in mock_cache.get_organizations()}

    created = list(
        process_data(
            config=test_config,
            positions=positions,
            employees=employees,
            orgunits=orgs,
            date_range=date_range,
        )
    )

    assert created == [Entry(**x) for x in mantall]


def test_process_data_pruned(
    mock_cache, mantall, pruned_forest, test_config, date_range
):
    """Check that test data produces correct output"""
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employees = mock_cache.get_employees()
    orgs = {str(x.id): x for x in pruned_forest[10000429].values()}

    created = list(
        process_data(
            config=test_config,
            positions=positions,
            employees=employees,
            orgunits=orgs,
            date_range=date_range,
            roots=[10000429],
        )
    )

    assert created == [Entry(**x) for x in mantall[:-1]]


def test_process_data_no_result_for_year_1001(mock_cache, mantall, test_config):
    """Check that test data produces correct output"""
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employees = mock_cache.get_employees()
    orgs = {str(i.id): i for i in mock_cache.get_organizations()}
    too_early = date(1001, 1, 1)
    created = list(
        process_data(
            config=test_config,
            positions=positions,
            employees=employees,
            orgunits=orgs,
            date_range=DateRange(start=too_early, end=too_early),
        )
    )

    assert created == []


def test_process_data_pruned_get_allowed_ids_is_empty(
    mock_cache, mantall, test_config, date_range
):
    """Check that test data produces correct output"""
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employees = mock_cache.get_employees()
    orgs = {str(i.id): i for i in mock_cache.get_organizations()}
    with pytest.raises(NoAccess):
        process_data(
            config=test_config,
            positions=positions,
            employees=employees,
            orgunits=orgs,
            roots=[10000429],
            get_allowed_ids=lambda x: set(),
            date_range=date_range,
        )


def test_process_data_pruned_no_allowed_ids_in_forest(
    mock_cache, mantall, test_config, date_range
):
    """Check that test data produces correct output"""
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employees = mock_cache.get_employees()
    orgs = {str(i.id): i for i in mock_cache.get_organizations()}
    with pytest.raises(NoAccess):
        process_data(
            config=test_config,
            positions=positions,
            employees=employees,
            orgunits=orgs,
            roots=[10000429],
            get_allowed_ids=lambda x: set("IKKE HER"),
            date_range=date_range,
        )


def test_process_data_pruned_no_allowed_ids_is_root(
    mock_cache, mantall, test_config, date_range
):
    """Check that test data produces correct output"""
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employees = mock_cache.get_employees()
    orgs = {i.id: i for i in mock_cache.get_organizations()}
    root = 10000429

    created = list(
        process_data(
            config=test_config,
            positions=positions,
            employees=employees,
            orgunits=orgs,
            roots=[root],
            get_allowed_ids=lambda x: {10000429},
            date_range=date_range,
        )
    )

    assert created == [Entry(**x) for x in mantall[:-1]]


def test_process_data_illegal_mg(mock_cache, test_config, date_range):
    """Check that test data produces correct output"""
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    orgs = {i.id: i for i in mock_cache.get_organizations()}
    employees = [
        x.copy(update={"medarbeidergruppe": test_config.illegal_mg[0]})
        for x in mock_cache.get_employees()
    ]

    created = list(
        process_data(
            config=test_config,
            positions=positions,
            employees=employees,
            orgunits=orgs,
            date_range=date_range,
        )
    )

    assert created == []


def test_process_data_illegal_mug(mock_cache, test_config, date_range):
    """Check that test data produces correct output"""
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    orgs = {i.id: i for i in mock_cache.get_organizations()}
    employees = [
        x.copy(update={"medarbeiderundergruppe": test_config.illegal_mug[0]})
        for x in mock_cache.get_employees()
    ]

    created = list(
        process_data(
            config=test_config,
            positions=positions,
            employees=employees,
            orgunits=orgs,
            date_range=date_range,
        )
    )

    assert created == []


def test_get_and_generate_data(
    requests_mock, mock_context, positions, employees, orgs, mantall, date_range
):
    """
    Check that clients work as expected with config and file is written as expected
    """
    # Mock requests
    requests_mock.get("https://sap.example.com/ansatte/v1/", json=employees)
    requests_mock.get("https://sap.example.com/stillinger/v1/", json=positions)
    requests_mock.get("https://sap.example.com/organisasjoner/v1/", json=orgs)

    # Produce the data
    output = list(
        generate_data(
            context=mock_context,
            date_range=date_range,
        )
    )
    # Check that it matches expected output
    assert output == [Entry(**x) for x in mantall]


def test_uit_fields(mock_context, positions, employees, orgs, date_range):
    mock_context.config.include_uit_fields = True

    # Produce the data
    output = list(generate_data(context=mock_context, date_range=date_range))

    dict_employees = {x.get("id"): x for x in employees.get("ansatt")}

    def t(entry):
        if (employment := dict_employees.get(entry.ansattnr)) and employment.get(
            "stillingId"
        ) == entry.stilling_id:
            assert entry.hovedarbeidsforhold == "H"
        else:
            assert entry.hovedarbeidsforhold == "T"

    for x in output:
        t(x)


def test_employee_mapper_map_position(
    mock_cache, employees, test_config, forest, mantall, today
):
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employee = Ansatt(**employees["ansatt"][0])

    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entry = mapper.map_position(
        employee=employee,
        position=positions[str(employee.stilling_id)],
    )

    assert entry == Entry(**mantall[0])


def test_employee_mapper_map_employee(
    mock_cache, employees, test_config, forest, mantall, today
):
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employee = Ansatt(**employees["ansatt"][0])

    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entries = mapper.map_employee(employee=employee)

    assert list(entries) == [Entry(**mantall[0])]


def test_employee_mapper_map_employee_with_multi_stilling(
    mock_cache,
    test_config,
    forest,
    employee_w_multi_stilling,
    employee_w_multi_stilling_entries,
    today,
):
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employee = Ansatt(**employee_w_multi_stilling["ansatt"][0])
    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entries = mapper.map_employee(employee=employee)

    exp_multi_stilling_entries = [Entry(**x) for x in employee_w_multi_stilling_entries]

    assert list(entries) == exp_multi_stilling_entries


def test_employee_mapper_map_employee_with_multi_stilling_invalid_employee(
    mock_cache,
    test_config,
    forest,
    employee_w_multi_stilling,
    today,
):
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employee = Ansatt(**employee_w_multi_stilling["ansatt"][0])
    employee.fornavn = None
    employee.etternavn = None
    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entries = mapper.map_employee(employee=employee)

    assert list(entries) == []


def test_employee_mapper_map_employee_with_multi_stilling_min_dellonnsprosent(
    mock_cache,
    test_config,
    forest,
    employee_w_multi_stilling,
    today,
):
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employee = Ansatt(**employee_w_multi_stilling["ansatt"][0])

    # set dellonssprosent to low value
    employee.dellonnsprosent = 10

    def _set_dellonnsprosent(tilleggstilling, val):
        tilleggstilling.dellonnsprosent = val
        return tilleggstilling

    employee.tilleggsstilling = map(
        lambda ts: _set_dellonnsprosent(ts, 5),
        employee.tilleggsstilling,
    )

    # Act
    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entries = mapper.map_employee(employee=employee)

    # Assert
    assert len(list(entries)) == 0


def test_employee_mapper_map_employee_with_multi_stilling_not_in_forest(
    mock_cache,
    test_config,
    forest,
    employee_w_multi_stilling,
    today,
):
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employee = Ansatt(**employee_w_multi_stilling["ansatt"][0])

    # set stilling id to one which is not in forest
    def _set_stilling_id(tilleggstilling, val):
        tilleggstilling.stilling_id = val
        return tilleggstilling

    employee.tilleggsstilling = map(
        lambda ts: _set_stilling_id(ts, 5 + randrange(1000)),
        employee.tilleggsstilling,
    )
    # set main stilling dellonnsprosen to lower value
    employee.dellonnsprosent = 40

    # Act
    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entries = mapper.map_employee(employee=employee)

    # Assert
    assert len(list(entries)) == 0


def test_employee_mapper_map_employee_with_multi_stilling_dellonnsprosent_is_none(
    mock_cache,
    test_config,
    forest,
    employee_w_multi_stilling,
    employee_w_multi_stilling_entries,
    today,
):
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employee_w_multi_stilling["ansatt"][0]["dellonnsprosent"] = None
    employee_w_multi_stilling_entries[0]["dellonnsprosent"] = 50
    employee = Ansatt(**employee_w_multi_stilling["ansatt"][0])

    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entries = list(mapper.map_employee(employee=employee))

    assert entries == [Entry(**x) for x in employee_w_multi_stilling_entries]


def test_employee_mapper_map_employee_invalid_employee(
    mock_cache, employees, test_config, forest, mantall, today
):
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    invalid_employee = {**employees["ansatt"][0], "kjonn": None}
    employee = Ansatt.to_model_class_with_optional_fields()(**invalid_employee)

    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entries = mapper.map_employee(employee=employee)

    with pytest.raises(ValueError):
        Ansatt(**invalid_employee)
    assert list(entries) == [Entry(**mantall[0])]


def test_employee_mapper_map_employee_unknown_position(
    mock_cache, test_config, forest, caplog, today
):
    caplog.set_level(level=logging.ERROR, logger=generate_mfe_logger.name)
    employee = next(mock_cache.get_employees())

    mapper = EmployeeMapper(
        config=test_config,
        positions={},
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entries = mapper.map_employee(employee=employee)

    assert list(entries) == []
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.ERROR
    assert (
        caplog.records[0].message
        == f"Position with id `{employee.stilling_id}` not found for employee `{employee.id}`"
    )


def test_employee_mapper_map_position_unknown_position(
    mock_cache, test_config, forest, caplog, today
):
    caplog.set_level(level=logging.ERROR, logger=generate_mfe_logger.name)
    employee = next(mock_cache.get_employees())
    positions = {str(x.id): x for x in mock_cache.get_positions()}
    position = positions[str(employee.stilling_id)]
    position.id = "Fins ikke"

    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )
    entry = mapper.map_position(employee=employee, position=position)

    assert entry is None
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.ERROR
    assert (
        caplog.records[0].message
        == f"Position with id `{position.id}` not found for employee `{employee.id}`"
    )


def test_employee_mapper_map_employee_unknown_multi_position(
    mock_cache, test_config, forest, caplog, employee_w_multi_stilling, today
):
    test_config.add_all_dellonnsprosent = True
    caplog.set_level(level=logging.ERROR, logger=generate_mfe_logger.name)
    employee = Ansatt(**employee_w_multi_stilling["ansatt"][0])
    positions = {str(x.id): x for x in mock_cache.get_positions()}
    employee.tilleggsstilling[0].stilling_id = "Fins ikke"
    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        date_range=DateRange(start=today, end=today),
    )

    entries = list(mapper.map_employee(employee=employee))

    assert len(entries) == 2
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.ERROR
    assert (
        caplog.records[0].message
        == f"Position with id `Fins ikke` not found for employee `{employee.id}`"
    )


@pytest.mark.parametrize(
    ("start_delta", "end_delta"),
    (
        # startdato and sluttdato in range
        (timedelta(), timedelta()),
        # startdato before range and sluttdato after range
        (timedelta(days=-1), timedelta(days=1)),
        # startdato in range and sluttdato after range
        (timedelta(), timedelta(days=1)),
        # startdato before range and sluttdato in range
        (timedelta(days=-1), timedelta()),
    ),
)
def test_employee_mapper_is_position_in_range_true(
    test_config, today, start_delta, end_delta
):
    mapper = EmployeeMapper(
        config=test_config,
        positions={},
        forest={},
        date_range=DateRange(start=today, end=today),
    )
    pos = SimpleNamespace(startdato=today + start_delta, sluttdato=today + end_delta)

    assert mapper.is_position_in_range(pos)  # noqa


@pytest.mark.parametrize(
    ("delta",),
    (
        # startdato and sluttdato after range
        (timedelta(days=1),),
        # startdato and sluttdato before range
        (timedelta(days=-1),),
    ),
)
def test_employee_mapper_is_position_in_range_false(test_config, today, delta):
    mapper = EmployeeMapper(
        config=test_config,
        positions={},
        forest={},
        date_range=DateRange(start=today, end=today),
    )
    pos = SimpleNamespace(startdato=today + delta, sluttdato=today + delta)

    assert not mapper.is_position_in_range(pos)  # noqa


def test_employee_mapper_is_position_in_range_raises_value_error(
    test_config, today, caplog
):
    mapper = EmployeeMapper(
        config=test_config,
        positions={},
        forest={},
        date_range=DateRange(start=today, end=today),
    )
    pos = SimpleNamespace(startdato=today, sluttdato=today - timedelta(days=1))
    caplog.set_level(level=logging.INFO, logger=generate_mfe_logger.name)

    assert not mapper.is_position_in_range(pos)  # noqa
    assert (
        caplog.records[0].message
        == f"Invalid date range: {pos.sluttdato} < {pos.startdato}"
    )


def test_make_terminovervakning_dict(mock_cache, terminovervakning_dict):
    assert _make_terminovervakning_dict(mock_cache) == terminovervakning_dict
    for k, xs in terminovervakning_dict.items():
        assert all(k == x.id for x in xs)


@pytest.mark.parametrize(
    "employee_id,termintype,expected_avtaledato",
    [
        ("00102992", Termintype.hovedstilling, "2022-12-31"),
        ("00102992", Termintype.tilleggsstilling, "2021-12-31"),
        ("00102989", Termintype.hovedstilling, "2021-01-01"),
        ("00102989", Termintype.tilleggsstilling, object()),
    ],
)
def test__extract_latest_terminovervakning(
    employee_id, termintype, expected_avtaledato, terminovervakning_dict
):
    x = _extract_latest_terminovervakning(
        termintype, terminovervakning_dict[employee_id]
    )
    if employee_id == "00102989" and termintype == Termintype.tilleggsstilling:
        assert x is None
    else:
        assert x.id == employee_id
        assert x.termintype == termintype
        assert str(x.avtaledato) == expected_avtaledato


def test__extract_latest_terminovervakning_empty():
    assert _extract_latest_terminovervakning(None, ()) is None


def test_employee_mapper_map_employee_with_multi_stilling_add_terminovervakning(
    mock_cache,
    test_config,
    forest,
    employee_w_multi_stilling,
    employee_with_tilleggstilling_and_terminovervakning_entries,
    terminovervakning_dict,
    today,
):
    test_config.add_terminovervakning = True
    positions = {str(i.id): i for i in mock_cache.get_positions()}
    employee = Ansatt(**employee_w_multi_stilling["ansatt"][0])

    mapper = EmployeeMapper(
        config=test_config,
        positions=positions,
        forest=forest,
        terminovervakning=terminovervakning_dict,
        date_range=DateRange(start=today, end=today),
    )
    entries = list(mapper.map_employee(employee=employee))

    assert entries == [
        Entry(**x) for x in employee_with_tilleggstilling_and_terminovervakning_entries
    ]
