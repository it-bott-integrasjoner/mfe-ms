import json
from mfe_ms.cache import Cache
import time
from mfe_ms.context import get_context

context = get_context("./../config.yaml")
cache = context.cache
redis = cache.redis

# A small tool to fill redis cache with fixture data, or to read it back
# Because of the path to the config.yaml file, you should be in this tests folder when running this script.


def fill_redis():
    with open("fixtures/emps.json") as jsonfile:
        employees = json.load(jsonfile)

    with open("fixtures/pos.json") as jsonfile:
        positions = json.load(jsonfile)

    with open("fixtures/orgs.json") as jsonfile:
        orgs = json.load(jsonfile)

    with redis.pipeline() as pipe:
        pipe.set(Cache.Key.EMPLOYEES, json.dumps(employees["ansatt"]))
        pipe.set(Cache.Key.POSITIONS, json.dumps(positions["stilling"]))
        pipe.set(Cache.Key.ORGANIZATIONS, json.dumps(orgs["organisasjon"]))
        pipe.set(Cache.Key.LAST_UPDATE, int(time.time()))
        pipe.execute()


def load_redis():
    cached_positions = cache.redis.get(Cache.Key.POSITIONS)
    print(cached_positions)
    cached_employees = cache.redis.get(Cache.Key.EMPLOYEES)
    print(cached_employees)
    cached_orgunits = cache.redis.get(Cache.Key.ORGANIZATIONS)
    print(cached_orgunits)


if __name__ == "__main__":
    print("Writing fixture data to redis.")
    fill_redis()
    print("Loading data from redis:")
    load_redis()
