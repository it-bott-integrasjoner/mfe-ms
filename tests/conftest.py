import datetime
import json
import os
from uuid import UUID

import pytest
import requests_mock
from authlib.integrations.starlette_client import OAuth
from bottint_tree import Tree
from dfo_sap_client import SapClient
from dfo_sap_client.models import Leder, Orgenhet, Terminovervakning

from mfe_ms import config
from mfe_ms.cache import Cache
from mfe_ms.config import (
    CONFIG_ENVIRON,
    AuthConfig,
    AuthorizationType,
    DataportenConfig,
    DfoSapConfig,
    DfoSapEndpoints,
    EntitlementsAuthorization,
    Instance,
    MFEConfig,
)
from mfe_ms.context import MFEContext
from mfe_ms.date_range import DateRange
from mfe_ms.tree import OrganizationForestT

os.environ[CONFIG_ENVIRON] = "config.example.yaml"
for k in os.environ:
    if k.startswith("SENTRY"):
        # Clear Sentry specific environment variables as they
        # may interfere with some tests
        del os.environ[k]


@pytest.fixture
def non_existing_root():
    return "1"


@pytest.fixture
def root_org(forest):
    return next(x for x in forest if forest[x].parent is None)


@pytest.fixture
def mdate():
    return datetime.date(year=2021, month=11, day=1)


@pytest.fixture
def mantall():
    with open("tests/fixtures/mantall.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def mantall_10000433():
    with open("tests/fixtures/manntall_10000433.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def positions():
    with open("tests/fixtures/pos.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def employees():
    with open("tests/fixtures/emps.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def employee_w_multi_stilling():
    with open("tests/fixtures/emp_w_tilleggstilling.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def employee_w_multi_stilling_entries():
    with open("tests/fixtures/emp_w_tilleggstilling_entries.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def employee_with_tilleggstilling_and_terminovervakning_entries():
    with open(
        "tests/fixtures/emp_w_tilleggstilling_and_terminovervakning_entries.json"
    ) as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def terminovervakning():
    with open("tests/fixtures/terminovervakning.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def terminovervakning_dict():
    with open("tests/fixtures/terminovervakning_dict.json") as jsonfile:
        return {
            k: [Terminovervakning(**x) for x in xs]
            for k, xs in json.load(jsonfile).items()
        }


@pytest.fixture
def orgs():
    with open("tests/fixtures/orgs.json") as jsonfile:
        return json.load(jsonfile)


def _load_forest(filtename: str) -> OrganizationForestT:
    with open(filtename) as fh:
        forest = eval(
            fh.read(),
            {},
            {
                "__builtins__": None,
                "Tree": Tree,
                "Orgenhet": Orgenhet,
                "Leder": Leder,
            },
        )
    for k, v in forest.items():
        v.parent = forest.get(v.data.overordn_orgenhet_id)
        v.children = list(forest[x.id] for x in v.children)
    return forest


@pytest.fixture
def forest():
    return _load_forest("tests/fixtures/forest.dat")


@pytest.fixture
def pruned_forest():
    return _load_forest("tests/fixtures/pruned_forest.dat")


@pytest.fixture
def test_config_loader():
    dataporten = DataportenConfig(client_id="foo", client_secret="bar")
    auth = AuthConfig(
        secret_key="nonsense for testing",
        dataporten=dataporten,
        authorization_type=AuthorizationType.entitlements,
        authorization_config=EntitlementsAuthorization(
            allowed_super_user_access=set(["urn:mace:uio.no:evalg:valgadministrator"]),
            allowed_org_user_entitlement_prefix="urn:mace:uio.no:mfe-test:valgadministrator:",
        ),
        api_key=UUID("00000000-0000-0000-0000-000000000000"),
    )
    sap_urls = DfoSapEndpoints(
        baseurl="https://0.0.0.0/sap",
        ansatt_url=None,
        orgenhet_url=None,
        stilling_url=None,
        ansatte_terminovervakning=None,
    )
    sap = DfoSapConfig(urls=sap_urls, tokens=None, headers=None)
    mfe_ms = MFEConfig(
        instance_name=Instance.uio,
        illegal_mg=("9", "09"),
        illegal_mug=("4", "04", "15", "30"),
        auth=auth,
        sap=sap,
        add_all_dellonnsprosent=True,
    )
    return config.MFEMsConfigLoader(mfe_ms=mfe_ms, logging=None, sentry=None)


@pytest.fixture
def test_config(test_config_loader):
    return test_config_loader.mfe_ms


@pytest.fixture
def auth_config_lists():
    c = {
        "secret_key": "create secret_key with 'openssl rand -hex 32'",
        "dataporten": {
            "client_id": "Get from Dataporten dashbpard",
            "client_secret": "Get from Dataporten dashbpard",
        },
        "authorization_type": "lists",
        "authorization_config": {
            "allowed_users": ["feide:asbjorn_elevg@spusers.feide.no"],
            "allowed_groups": [
                "fc:org:spusers.feide.no",
                "fc:adhoc:f3b45c60-4717-439f-95ae-5254b491cc83",
            ],
        },
    }
    return config.AuthConfig(**c)


@pytest.fixture
def test_config_list_authentication(test_config, auth_config_lists):
    test_config.auth = auth_config_lists
    return test_config


@pytest.fixture
def auth_config_entitlements():
    c = {
        "secret_key": "create secret_key with 'openssl rand -hex 32'",
        "dataporten": {
            "client_id": "Get from Dataporten dashbpard",
            "client_secret": "Get from Dataporten dashbpard",
        },
        "authorization_type": "entitlements",
        "authorization_config": {
            "allowed_super_user_access": ["urn:mace:uio.no:evalg:valgadministrator"],
            "allowed_org_user_entitlement_prefix": "urn:mace:uio.no:mfe-test:valgadministrator:",
        },
    }
    return config.AuthConfig(**c)


@pytest.fixture
def test_config_entitlement_authentication(test_config, auth_config_entitlements):
    test_config.auth = auth_config_entitlements
    return test_config


@pytest.fixture
def mock_cache(positions, employees, orgs, terminovervakning):
    class MockCache(Cache):
        def __init__(self):
            self.redis = {
                Cache.Key.EMPLOYEES: json.dumps(employees["ansatt"]),
                Cache.Key.POSITIONS: json.dumps(positions["stilling"]),
                Cache.Key.ORGANIZATIONS: json.dumps(orgs["organisasjon"]),
                Cache.Key.TERMINOVERVAKNING: json.dumps(
                    terminovervakning["AnsattTermin"]
                ),
                Cache.Key.LAST_UPDATE: "1500000000",
            }

    return MockCache()


@pytest.fixture
def mock_context(mock_cache, test_config_loader):
    cache = mock_cache  # noqa
    sap = SapClient(
        **test_config_loader.mfe_ms.sap.dict(),
        ignore_required_model_fields=True,
    )
    oauth = OAuth()
    return MFEContext(sap, cache, oauth, test_config_loader)


@pytest.fixture
def mock_api():
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture
def openid_configuration():
    with open("tests/fixtures/openid-configuration.json") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def today():
    return datetime.date(year=2022, month=1, day=1)


@pytest.fixture
def future_date():
    return datetime.date(year=9000, month=1, day=1)


@pytest.fixture
def date_range(today, future_date):
    return DateRange(start=today, end=future_date)
