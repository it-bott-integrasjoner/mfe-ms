import os

import pytest

from mfe_ms.config import (
    MFEMsConfigLoader,
    MFEConfig,
    get_config,
    CONFIG_ENVIRON,
    iter_config_locations,
    CONFIG_NAME,
    AuthConfig,
    ListsAuthorization,
)


@pytest.fixture
def example_config_path():
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    return os.path.join(here, "..", "config.example.yaml")


def test_config_loader(example_config_path):
    with open(example_config_path) as f:
        example_config_yamlstr = f.read()
    config_from_yamlstr = MFEMsConfigLoader.from_yaml(example_config_yamlstr)
    config_from_file = MFEMsConfigLoader.from_file(example_config_path)
    assert config_from_file == config_from_yamlstr
    assert isinstance(config_from_file, MFEMsConfigLoader)
    assert isinstance(config_from_file.mfe_ms, MFEConfig)


def test_get_config(example_config_path):
    config = get_config(filename=example_config_path)
    assert isinstance(config, MFEConfig)


def test_iter_config_locations_lookup_order():
    del os.environ[CONFIG_ENVIRON]
    confdir = "/test/path/to/"
    expected = confdir + CONFIG_NAME
    got = next(iter_config_locations(lookup_order=[confdir]))
    assert got == expected


def test_iter_config_locations():
    expected = "/test/path/to/" + CONFIG_NAME
    os.environ[CONFIG_ENVIRON] = expected
    got = next(iter_config_locations())
    os.unsetenv(CONFIG_ENVIRON)
    assert got == expected


def test_auth_config_allowed_is_both_none(auth_config_lists):
    with pytest.raises(ValueError):
        AuthConfig(dataporten=auth_config_lists.dataporten)


def test_auth_config_allowed_is_both_empty(test_config):
    with pytest.raises(
        ValueError,
        match=r"allowed_users and allowed_groups can't both be empty or None",
    ):
        AuthConfig(
            dataporten=test_config.auth.dataporten,
            secret_key="a",
            authorization_type="lists",
            authorization_config=ListsAuthorization(
                allowed_users=set(),
                allowed_groups=set(),
            ),
        )


def test_auth_config_allowed_users_is_none(test_config):
    groups = {"a", "b", "c"}

    a = AuthConfig(
        dataporten=test_config.auth.dataporten,
        secret_key="a",
        authorization_type="lists",
        authorization_config=ListsAuthorization(
            allowed_users=None,
            allowed_groups=groups,
        ),
    )

    assert a.authorization_config.allowed_users == []
    assert a.authorization_config.allowed_groups == groups


def test_auth_config_allowed_groups_is_none(test_config):
    users = {"a", "b", "c"}

    a = AuthConfig(
        dataporten=test_config.auth.dataporten,
        secret_key="a",
        authorization_type="lists",
        authorization_config=ListsAuthorization(
            allowed_users=users,
            allowed_groups=None,
        ),
    )

    assert a.authorization_config.allowed_groups == []
    assert a.authorization_config.allowed_users == users


def test_auth_config_allowed_groups_is_none_for_not_used_config(test_config):
    users = {"a", "b", "c"}

    a = AuthConfig(
        dataporten=test_config.auth.dataporten,
        secret_key="a",
        authorization_type="lists",
        authorization_config=ListsAuthorization(
            allowed_users=users,
            allowed_groups=["super-user-groups"],
        ),
    )

    assert a.authorization_config.allowed_groups == {"super-user-groups"}
    assert a.authorization_config.allowed_users == users
