import logging

import pytest
from dfo_sap_client.models import Orgenhet, Ansatt, Stilling
from redis import Redis

from mfe_ms.cache import logger as cache_logger, Cache, CacheItemNotFound


@pytest.fixture
def empty_cache():
    class MockCache(Cache):
        def __init__(self):
            self.redis = {}

    return MockCache()


def test_get_organizations(mock_cache, caplog):
    caplog.set_level(level=logging.ERROR, logger=cache_logger.name)

    xs = list(mock_cache.get_organizations())

    for x in xs:
        assert isinstance(x, Orgenhet)
    assert len(xs) == 6
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.ERROR
    assert caplog.records[0].message.startswith(
        "Invalid data from SAP, please notify the data owners: dropping <class 'dfo_sap_client.models.Orgenhet'> (id=10000434)"
    )


def test_get_employees(mock_cache, caplog):
    caplog.set_level(level=logging.WARNING, logger=cache_logger.name)

    xs = mock_cache.get_employees()

    for x in xs:
        assert isinstance(x, Ansatt)
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.WARNING
    assert caplog.records[0].message.startswith(
        "Validation error: 10 validation errors for Ansatt"
    )


def test_get_positions(mock_cache, caplog):
    caplog.set_level(level=logging.WARNING, logger=cache_logger.name)

    xs = mock_cache.get_positions()

    for x in xs:
        assert isinstance(x, Stilling)
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.WARNING
    assert caplog.records[0].message.startswith(
        "Validation error: 1 validation error for Stilling"
    )


def test_get_positions_empty_cache_raise_not_found_is_true(empty_cache):
    with pytest.raises(CacheItemNotFound) as e:
        empty_cache.get_positions(raise_not_found=True)
    assert e.value.args == (Cache.Key.POSITIONS,)


def test_get_positions_empty_cache_raise_not_found_is_false(empty_cache):
    assert empty_cache.get_positions(raise_not_found=False) is None


def test_get_employees_empty_cache_raise_not_found_is_true(empty_cache):
    with pytest.raises(CacheItemNotFound) as e:
        empty_cache.get_employees(raise_not_found=True)
    assert e.value.args == (Cache.Key.EMPLOYEES,)


def test_get_employees_empty_cache_raise_not_found_is_false(empty_cache):
    assert empty_cache.get_employees(raise_not_found=False) is None


def test_get_organizations_empty_cache_raise_not_found_is_true(empty_cache):
    with pytest.raises(CacheItemNotFound) as e:
        empty_cache.get_organizations(raise_not_found=True)
    assert e.value.args == (Cache.Key.ORGANIZATIONS,)


def test_get_organizations_empty_cache_raise_not_found_is_false(empty_cache):
    assert empty_cache.get_organizations(raise_not_found=False) is None


def test___init__(test_config):
    cache = Cache(test_config.cache)

    assert cache.config == test_config.cache
    assert isinstance(cache.redis, Redis)
    assert (
        cache.redis.connection_pool.connection_kwargs["password"]
        == test_config.cache.password
    )
    assert (
        cache.redis.connection_pool.connection_kwargs["port"] == test_config.cache.port
    )
    assert (
        cache.redis.connection_pool.connection_kwargs["host"] == test_config.cache.host
    )
