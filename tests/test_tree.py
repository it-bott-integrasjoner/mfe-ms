from dfo_sap_client.models import Orgenhet

from mfe_ms.tree import build_organization_forest, prune, clean


def test_build_organization_forest(mock_cache, forest):
    orgs = mock_cache.get_organizations()

    trees = build_organization_forest(orgs)
    assert trees == forest


def test_prune(forest, pruned_forest):
    pruned = {x.id: x for x in prune(forest[10000429])}

    assert pruned == pruned_forest


def test_clean_allowed_from_different_branches(forest):
    allowed = {10000431, 10000433}
    cleaned = clean(forest, allowed)

    assert cleaned.keys() == allowed
    assert all(isinstance(x.data, Orgenhet) for x in cleaned.values())
    assert all(x.parent is None for x in cleaned.values())


def test_clean_parent_and_child_in_allowed(forest):
    allowed = {10000429, 10000431}

    cleaned = clean(forest, allowed)
    new_root = cleaned[10000429]

    assert cleaned.keys() == {*allowed, 10000432, 10000433}
    assert all(isinstance(x.data, Orgenhet) for x in cleaned.values())
    assert all(x.parent == new_root for x in cleaned.values() if x is not new_root)
    assert new_root.parent is None


def test_clean_allowed_is_empty(forest):
    allowed = set()
    cleaned = clean(forest, allowed)

    assert not cleaned
