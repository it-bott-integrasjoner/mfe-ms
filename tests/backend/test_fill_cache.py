import json

from dfo_sap_client.models import Ansatt, Stilling, Orgenhet, Terminovervakning

from mfe_ms.backend.fill_cache import (
    _get_all_stillinger_as_json,
    _get_ansatte_terminovervakning,
    _get_all_orgenheter_as_json,
    _get_all_employees_as_json,
)


def test_get_employees_as_json(mock_context, requests_mock, employees):
    requests_mock.get("https://0.0.0.0/ansatt/", json=employees)
    cached = _get_all_employees_as_json(mock_context.sap)

    original_objs = [
        Ansatt.to_model_class_with_optional_fields()(**x) for x in employees["ansatt"]
    ]
    cached_objs = [
        Ansatt.to_model_class_with_optional_fields()(**x) for x in json.loads(cached)
    ]

    assert original_objs == cached_objs


def test_get_all_stillinger_as_json(mock_context, requests_mock, positions):
    requests_mock.get("https://0.0.0.0/stilling/", json=positions)
    cached = _get_all_stillinger_as_json(mock_context.sap)
    original_objs = [
        Stilling.to_model_class_with_optional_fields()(**x)
        for x in positions["stilling"]
    ]
    cached_objs = [
        Stilling.to_model_class_with_optional_fields()(**x) for x in json.loads(cached)
    ]
    assert original_objs == cached_objs


def test_get_all_orgenheter_as_json(mock_context, requests_mock, orgs):
    requests_mock.get("https://0.0.0.0/orgenhet/", json=orgs)
    cached = _get_all_orgenheter_as_json(mock_context.sap)
    original_objs = [
        Orgenhet.to_model_class_with_optional_fields()(**x)
        for x in orgs["organisasjon"]
    ]
    cached_objs = [
        Orgenhet.to_model_class_with_optional_fields()(**x) for x in json.loads(cached)
    ]
    assert original_objs == cached_objs


def test_get_ansatte_terminovervakning(mock_context, requests_mock, terminovervakning):
    requests_mock.get(
        "https://0.0.0.0/ansatteTerminovervakning/", json=terminovervakning
    )
    cached = _get_ansatte_terminovervakning(mock_context.sap)
    original_objs = [
        Terminovervakning.to_model_class_with_optional_fields()(**x)
        for x in terminovervakning["AnsattTermin"]
    ]
    cached_objs = [
        Terminovervakning.to_model_class_with_optional_fields()(**x)
        for x in json.loads(cached)
    ]
    assert original_objs == cached_objs
