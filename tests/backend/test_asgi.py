import pytest

from contextlib import nullcontext

from fastapi import FastAPI
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from sentry_sdk.utils import BadDsn


@pytest.mark.parametrize(
    "sentry_config,expected_type",
    [
        ({"dsn": "invalid_dsn"}, BadDsn),
        ({"environment": "test"}, SentryAsgiMiddleware),
        (None, FastAPI),
        ({}, FastAPI),
    ],
)
def test_asgi(mock_context, sentry_config, expected_type):
    mock_context.config_loader.sentry = sentry_config
    try:
        if issubclass(expected_type, Exception):
            # We expect an exception to be raised
            contextmanager = pytest.raises(expected_type)
        else:
            contextmanager = nullcontext()

        with contextmanager:
            # load mfe_ms.backend.asgi
            import mfe_ms.backend.asgi as asgi_module

            assert type(asgi_module.create_app(mock_context)) == expected_type
    finally:
        # Reset sentry
        import sentry_sdk

        sentry_sdk.init(dsn="")
