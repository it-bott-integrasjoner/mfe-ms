# NB! frontend/dist/ must exist to be able to import api module
from http import HTTPStatus

from gunicorn.util import http_date

from mfe_ms.backend.auth import Scope
from .conftest import create_session_cookie

from mfe_ms.generate_mfe import Entry
from mfe_ms.backend.api import Error


def _compare_models(cls, a, b):
    return [cls(**x) for x in a] == [cls(**x) for x in b]


def test_get_manntall_no_auth_returns_unauthorized(mock_app, manntall_url):
    response = mock_app.client.get(manntall_url)
    assert response.status_code == HTTPStatus.UNAUTHORIZED


def test_get_manntall_with_session_superuser_ok(
    mock_app,
    mantall,
    manntall_url,
):
    cookie = create_session_cookie(mock_app, [Scope.superuser, Scope.authorized])

    response = mock_app.client.get(manntall_url, headers=({"cookie": cookie}))

    assert response.status_code == HTTPStatus.OK
    assert _compare_models(Entry, response.json(), mantall)
    assert response.headers["last-modified"] == http_date(
        mock_app.context.cache.last_update.timestamp()
    )


def test_get_manntall_10000433_with_session_ok(
    mock_app,
    mantall_10000433,
    manntall_url,
):
    cookie = create_session_cookie(
        mock_app, ["organization:10000433", Scope.authorized]
    )

    response = mock_app.client.get(manntall_url, headers=({"cookie": cookie}))

    assert response.status_code == HTTPStatus.OK
    assert _compare_models(Entry, response.json(), mantall_10000433)


def test_get_manntall_10000433_with_session_root_not_allowed(
    mock_app,
    manntall_url,
    root_org,
):
    cookie = create_session_cookie(
        mock_app, ["organization:10000433", Scope.authorized]
    )

    response = mock_app.client.get(
        f"{manntall_url}&root={root_org}", headers=({"cookie": cookie})
    )

    assert response.status_code == HTTPStatus.FORBIDDEN


def test_get_manntall_with_api_key_ok(mock_app, mantall, manntall_url):
    response = mock_app.client.get(
        manntall_url,
        headers={"authorization": f"Bearer {mock_app.context.config.auth.api_key}"},
    )

    assert response.status_code == HTTPStatus.OK
    assert _compare_models(Entry, response.json(), mantall)


def test_get_manntall_with_wrong_api_key_is_forbidden(mock_app, manntall_url):
    response = mock_app.client.get(
        manntall_url,
        headers={"authorization": "Bearer Nope!"},
    )
    assert response.status_code == HTTPStatus.FORBIDDEN
    assert Error(**response.json()) == Error(
        detail="Forbidden", status=response.status_code
    )


def test_get_manntall_with_non_existing_root_returns_404(
    mock_app, mantall, manntall_url, non_existing_root
):
    response = mock_app.client.get(
        f"{manntall_url}&root={non_existing_root}",
        headers={"authorization": f"Bearer {mock_app.context.config.auth.api_key}"},
    )

    assert response.status_code == HTTPStatus.NOT_FOUND
    assert Error(**response.json()) == Error(
        detail=non_existing_root, status=response.status_code
    )


def test_api_docs_always_returns_200(mock_app):
    response = mock_app.client.get("/api/docs")

    assert response.status_code == HTTPStatus.OK


def test_api_health(mock_app):
    response = mock_app.client.get("/health")
    assert response.status_code == HTTPStatus.OK
    assert "metadata" in response.json()
