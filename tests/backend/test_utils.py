import datetime

from mfe_ms.backend.utils import get_allowed_organizations_for_scopes
from mfe_ms.backend.routes.dataporten import User
from mfe_ms.tree import build_organization_forest


def _test_user(scopes):
    now = datetime.datetime.utcnow()

    return User(
        id="userid",
        name="Test Testson",
        issued_at=now,
        expires_at=now + datetime.timedelta(hours=1),
        scopes=scopes,
    )


def test_get_allowed_organizations_for_scopes_superuser(mock_cache):
    user = _test_user(scopes=["superuser"])
    orgs = mock_cache.get_organizations()
    tree = build_organization_forest(orgs)
    assert get_allowed_organizations_for_scopes(user.scopes, tree) == {10000425}


def test_get_allowed_organizations_for_scopes(mock_cache):
    allowed = {10000433, 10000429}
    scopes = [f"organization:{x}" for x in allowed]
    user = _test_user(scopes=scopes)
    orgs = mock_cache.get_organizations()
    tree = build_organization_forest(orgs)

    assert get_allowed_organizations_for_scopes(user.scopes, tree) == allowed


def test_get_allowed_organizations_for_scopes_no_allowed(mock_cache):
    user = _test_user(scopes=[])
    orgs = mock_cache.get_organizations()
    tree = build_organization_forest(orgs)

    assert get_allowed_organizations_for_scopes(user.scopes, tree) == set()
