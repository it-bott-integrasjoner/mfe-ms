from datetime import datetime, timedelta
import json
from base64 import b64encode
from typing import NamedTuple

import pytest
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from httpx import URL, BaseTransport, Response
from jose import jwt
from starlette.middleware.sessions import SessionMiddleware
from starlette.testclient import TestClient

import mfe_ms.backend.api
from mfe_ms.backend.routes.dataporten import User
from mfe_ms.context import MFEContext


class MockClient(TestClient):
    def request(self, *args, **kwargs) -> Response:
        return super().request(*args, **{**kwargs, "follow_redirects": False})  # type: ignore[arg-type]

    def _transport_for_url(self, url: URL) -> BaseTransport:
        # Workaround to get the correct transport
        return self._transport


class MockApp(NamedTuple):
    app: FastAPI
    context: MFEContext

    @property
    def client(self) -> MockClient:
        return MockClient(app=self.app)


@pytest.fixture
def mock_app(mock_context, httpx_mock, openid_configuration):
    return MockApp(
        app=mfe_ms.backend.api.init(mock_context)[0],
        context=mock_context,
    )


@pytest.fixture
def manntall_url(mdate):
    return f"/api/manntall?mdate={mdate}"


def create_session_cookie(mock_app, scopes):
    context = mock_app.context
    session_middleware = SessionMiddleware(mock_app.app, context.config.auth.secret_key)
    now = datetime.now()
    user = User(
        id="id",
        name="Testuser",
        issued_at=now,
        expires_at=now + timedelta(hours=1),
        scopes=scopes,
    )
    token = jwt.encode(
        dict(
            iss="mfe-ms",
            sub=user.json(),
            exp=user.expires_at,
            scope=user.scopes,
        ),
        key=context.config.auth.secret_key,
        algorithm=context.config.auth.algorithm,
    )
    data = b64encode(
        json.dumps(jsonable_encoder({"user": user, "token": token})).encode("utf-8")
    )
    data = session_middleware.signer.sign(data)
    return "%s=%s; path=/; Max-Age=%d; %s" % (
        session_middleware.session_cookie,
        data.decode("utf-8"),
        session_middleware.max_age,
        session_middleware.security_flags,
    )


@pytest.fixture
def openid_configuration_response(httpx_mock, test_config, openid_configuration):
    httpx_mock.add_response(
        url=test_config.auth.dataporten.well_known_url, json=openid_configuration
    )
