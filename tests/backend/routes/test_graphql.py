import pytest

import mfe_ms.backend.routes.graphql as graphql


@pytest.fixture
def schema_graphql():
    with open("tests/fixtures/schema.graphql") as f:
        return f.read()


def test_schema(mock_context, schema_graphql, capfd):
    graphql.print_schema(mock_context)
    out, _ = capfd.readouterr()
    assert out == schema_graphql
