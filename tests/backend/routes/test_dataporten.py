import re

import logging
from urllib.parse import parse_qs

import pydantic
import pytest
from pydantic import HttpUrl

from mfe_ms.backend.routes.dataporten import (
    is_authorized_by_group,
    is_authorized_by_id,
    is_authorized,
    logger as dataporten_logger,
    get_organization_scopes_for_user,
    has_super_user_access,
)
from mfe_ms.config import MFEConfig, EntitlementsAuthorization
from mfe_ms.context import MFEContext


def test_is_authorized_by_id_user_has_no_ids(test_config_list_authentication, caplog):
    caplog.set_level(level=logging.WARNING, logger=dataporten_logger.name)

    result = is_authorized_by_id(test_config_list_authentication, [])

    assert result is False
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.WARNING
    assert caplog.records[0].message == "Got empty list of ids, rejecting"


def test_is_authorized_by_id_not_in_allowed_users(
    test_config_list_authentication, caplog
):
    caplog.set_level(level=logging.INFO, logger=dataporten_logger.name)

    result = is_authorized_by_id(test_config_list_authentication, ["NOT ALLOWED"])

    assert result is False
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.INFO
    assert caplog.records[0].message == "Rejecting user: ['NOT ALLOWED']"


def test_is_authorized_by_id_allowed_users_is_none(
    test_config_list_authentication, caplog
):
    caplog.set_level(level=logging.INFO, logger=dataporten_logger.name)
    test_config_list_authentication.auth.authorization_config.allowed_users = None

    result = is_authorized_by_id(test_config_list_authentication, ["NOT ALLOWED"])

    assert result is True
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.INFO
    assert caplog.records[0].message == "`allowed_users` is None, accepting all"


def test_is_authorized_by_id_in_allowed_users(test_config_list_authentication, caplog):
    caplog.set_level(level=logging.INFO, logger=dataporten_logger.name)
    user = list(
        test_config_list_authentication.auth.authorization_config.allowed_users
    )[0]

    result = is_authorized_by_id(test_config_list_authentication, [user])

    assert result is True
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.INFO
    assert caplog.records[0].message == f"Accepting user: ['{user}']"


def test_is_authorized_by_group(test_config_list_authentication, mock_api):
    mock_api.get(
        test_config_list_authentication.auth.dataporten.groups_endpoint,
        json=[
            {
                "id": list(
                    test_config_list_authentication.auth.authorization_config.allowed_groups
                )[0]
            }
        ],
    )

    result = is_authorized_by_group(test_config_list_authentication, "")

    assert result is True
    assert mock_api.called_once


def test_is_authorized_by_group_whith_admin_access(
    test_config_list_authentication, mock_api
):
    mock_api.get(
        test_config_list_authentication.auth.dataporten.groups_endpoint,
        json=[
            {
                "id": list(
                    test_config_list_authentication.auth.authorization_config.allowed_groups
                )[0]
            }
        ],
    )

    result = is_authorized_by_group(test_config_list_authentication, "")

    assert result is True
    assert mock_api.called_once


def test_is_authorized_by_group_user_has_no_groups(
    test_config_list_authentication, mock_api
):
    mock_api.get(
        test_config_list_authentication.auth.dataporten.groups_endpoint, json=[]
    )

    result = is_authorized_by_group(test_config_list_authentication, "")

    assert result is False
    assert mock_api.called_once


def test_is_authorized_by_group_user_has_no_groups_allowed_groups_is_none(
    test_config_list_authentication, caplog, mock_api
):
    test_config_list_authentication.auth.authorization_config.allowed_groups = None
    caplog.set_level(level=logging.INFO, logger=dataporten_logger.name)
    mock_api.get(
        test_config_list_authentication.auth.dataporten.groups_endpoint, json=[]
    )

    result = is_authorized_by_group(test_config_list_authentication, "")

    assert result is True
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.INFO
    assert caplog.records[0].message == "`allowed_groups` is None, accepting all"
    assert not mock_api.called


def test_is_authorized_by_group_user_has_no_groups_allowed_groups_is_empty(
    test_config_list_authentication, caplog, mock_api
):
    test_config_list_authentication.auth.authorization_config.allowed_groups = set()
    caplog.set_level(level=logging.INFO, logger=dataporten_logger.name)
    mock_api.get(
        test_config_list_authentication.auth.dataporten.groups_endpoint, json=[]
    )

    result = is_authorized_by_group(test_config_list_authentication, "")

    assert result is False
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == logging.INFO
    assert caplog.records[0].message == "`allowed_groups` is empty, rejecting all"
    assert not mock_api.called


def test_is_authorized_is_allowed_by_id(test_config_list_authentication, mock_api):
    mock_api.get(
        test_config_list_authentication.auth.dataporten.groups_endpoint, json=[]
    )
    user = list(
        test_config_list_authentication.auth.authorization_config.allowed_users
    )[0]

    result = is_authorized_by_id(test_config_list_authentication, [user])

    assert result is True
    assert not mock_api.called


def test_has_super_user_access(test_config_entitlement_authentication):
    test_config_entitlement_authentication.auth.authorization_config.allowed_super_user_access = (
        "urn:mace:uio.no:mfe_test:valgadministrator:los"
    )
    entitlements = {
        "urn:mace:uio.no:mfe_test:valgadministrator:los",
        "urn:mace:uio.no:mfe_test:valgadministrator:los-hms",
        "urn:mace:uio.no:mfe_test:valgadministrator:los-eir",
    }

    super_user_access = has_super_user_access(
        test_config_entitlement_authentication, entitlements
    )

    assert super_user_access


def test_has_super_user_access_no_access(test_config_entitlement_authentication):
    test_config_entitlement_authentication.auth.authorization_config.allowed_super_user_access = (
        "urn:mace:uio.no:mfe_test:valgadministrator:super-unit"
    )
    entitlements = {
        "urn:mace:uio.no:mfe_test:valgadministrator:los",
        "urn:mace:uio.no:mfe_test:valgadministrator:los-hms",
        "urn:mace:uio.no:mfe_test:valgadministrator:los-eir",
    }

    super_user_access = has_super_user_access(
        test_config_entitlement_authentication, entitlements
    )

    assert not super_user_access


@pytest.mark.parametrize("ids", [[], ["unknown"]], ids=str)
def test_is_authorized_is_rejected_by_user_but_accepted_by_group(
    test_config_list_authentication, mock_api, ids
):
    mock_api.get(
        test_config_list_authentication.auth.dataporten.groups_endpoint,
        json=[{"id": "fc:org:spusers.feide.no"}],
    )

    result = is_authorized(test_config_list_authentication, ids, "")
    result_by_id = is_authorized_by_id(test_config_list_authentication, ids)
    result_by_group = is_authorized_by_group(test_config_list_authentication, "")

    assert result is True
    assert result_by_id is False
    assert result_by_group is True
    assert mock_api.call_count == 2


def test_get_organization_scopes_for_user(mock_context: MFEContext):
    fix_org_missing_field(mock_context)
    assert isinstance(
        mock_context.config.auth.authorization_config, EntitlementsAuthorization
    )
    mock_context.config.auth.authorization_config.allowed_org_user_entitlement_prefix = (
        "urn:mace:uio.no:mfe_test:valgadministrator:"
    )
    entitlements = {
        "urn:mace:uio.no:mfe_test:valgadministrator:los",
        "urn:mace:uio.no:mfe_test:valgadministrator:los-hms",
        "urn:mace:uio.no:mfe_test:valgadministrator:los-eir",
    }

    org_user_access_ids = get_organization_scopes_for_user(mock_context, entitlements)

    assert org_user_access_ids == {
        "organization:10000433",
        "organization:10000429",
        "organization:10000432",
    }


def fix_org_missing_field(mock_context):
    for org in mock_context.cache.get_organizations():
        if org.org_kortnavn is None:
            org.org_kortnavn = "LOS-SV"
            org.organisasjonsnavn = "Enhet for Samfunnsvitenskap"


def test_get_organization_scopes_for_user_none_orgs_user_access(
    mock_context, test_config_entitlement_authentication: MFEConfig
):
    fix_org_missing_field(mock_context)
    assert isinstance(
        test_config_entitlement_authentication.auth.authorization_config,
        EntitlementsAuthorization,
    )
    test_config_entitlement_authentication.auth.authorization_config.allowed_org_user_entitlement_prefix = (
        "urn:mace:uio.no:mfe:valgadministrator:"
    )
    entitlements = {
        "urn:mace:uio.no:mfe_test:valgadministrator:los",
        "urn:mace:uio.no:mfe_test:valgadministrator:los-hms",
        "urn:mace:uio.no:mfe_test:valgadministrator:los-eir",
    }

    org_user_access_ids = get_organization_scopes_for_user(mock_context, entitlements)

    assert not org_user_access_ids


def test_get_organization_scopes_for_user_raises_value_error(
    mock_context, test_config_entitlement_authentication: MFEConfig
):
    fix_org_missing_field(mock_context)
    assert isinstance(
        test_config_entitlement_authentication.auth.authorization_config,
        EntitlementsAuthorization,
    )
    test_config_entitlement_authentication.auth.authorization_config.allowed_org_user_entitlement_prefix = (
        "urn:mace:uio.no:mfe:valgadministrator:"
    )
    entitlements = {
        "urn:mace:uio.no:mfe:valgadministrator:los",
        "urn:mace:uio.no:mfe:valgadministrator:los-hms",
        "urn:mace:uio.no:mfe:valgadministrator:wrong",
    }
    error_msg = (
        "Invalid config in LDAP/Cerebrum, entitlement: "
        "urn:mace:uio.no:mfe:valgadministrator:wrong is not valid, "
        "Could not find any kortnavn/acronym: wrong in organization (from SAP)"
    )

    with pytest.raises(ValueError, match=re.escape(error_msg)):
        get_organization_scopes_for_user(mock_context, entitlements)


def test_dataporten_login(
    mock_app, openid_configuration, openid_configuration_response
):
    def get_first(d, key):
        values = d.get(key)
        if values:
            return values[0]

    mock_client = mock_app.client

    response = mock_client.get(
        mock_client.app.url_path_for("dataporten", path="/login"),
        follow_redirects=False,
    )
    redirect_url = pydantic.parse_obj_as(HttpUrl, response.headers.get("location"))
    query = parse_qs(redirect_url.query)

    assert get_first(query, "scope") == "openid"
    assert get_first(query, "response_type") == "code"
    assert get_first(query, "nonce")
    assert get_first(query, "state")
    assert get_first(query, "redirect_uri").endswith(
        mock_client.app.url_path_for("dataporten", path="/callback")
    )
    assert mock_client.cookies.get("session") is not None
    assert response.is_redirect is True
    assert (
        redirect_url.removesuffix(f"?{redirect_url.query}")
        == openid_configuration["authorization_endpoint"]
    )


def test_dataporten_logout(
    mock_app, openid_configuration, openid_configuration_response
):
    mock_client = mock_app.client
    response = mock_client.get(
        mock_client.app.url_path_for("dataporten", path="/logout"),
        follow_redirects=False,
    )
    redirect_url = pydantic.parse_obj_as(HttpUrl, response.headers.get("location"))

    assert mock_client.cookies.get("session") is None
    assert response.is_redirect is True
    assert (
        redirect_url.removesuffix(f"?{redirect_url.query}")
        == openid_configuration["end_session_endpoint"]
    )
