module Route exposing
    ( BasePath
    , Page(..)
    , Route
    , fromUrl
    , getBasePath
    , mkBasePath
    , toUrl
    )

import String
import Url exposing (Url)
import Url.Builder exposing (relative)
import Url.Parser as Parser exposing (Parser)


type Page
    = Home
    | NotFound


type BasePath
    = BasePath String


type alias Route =
    { page : Page, base : BasePath }


mkBasePath : String -> BasePath
mkBasePath s =
    if not (String.endsWith "/" s) then
        BasePath (s ++ "/")

    else
        BasePath s


getBasePath : BasePath -> String
getBasePath (BasePath path) =
    path


parser : Parser (Page -> b) b
parser =
    Parser.oneOf
        [ Parser.map Home Parser.top
        ]


fromUrl : BasePath -> Url -> Route
fromUrl ((BasePath basePath) as bp) url =
    { base = bp
    , page =
        { url | path = String.replace basePath "" url.path }
            |> Parser.parse parser
            |> Maybe.withDefault NotFound
    }


toUrl : Route -> String
toUrl { page, base } =
    let
        (BasePath basePath) =
            base

        path : String.String
        path =
            case page of
                Home ->
                    ""

                NotFound ->
                    "not-found"
    in
    -- BasePath always ends with a `/`, chop it off
    relative [ String.dropRight 1 basePath, path ] []
