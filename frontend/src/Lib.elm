module Lib exposing (ariaBusy, showBool)

import Html
import Html.Attributes as Attr


showBool : Bool -> String
showBool v =
    if v then
        "true"

    else
        "false"


ariaBusy : Bool -> Html.Attribute msg
ariaBusy busy =
    Attr.attribute "aria-busy" (showBool busy)
