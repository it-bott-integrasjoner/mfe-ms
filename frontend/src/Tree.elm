module Tree exposing (Tree(..), fromListWith, toListWith, view, viewItem)

import Html exposing (Attribute, Html)
import Html.Attributes as A
import Html.Attributes.Aria as Aria
import Html.Attributes.Extra as AE
import Html.Keyed


{-| A generic tree structure.
-}
type Tree a
    = Tree a (Maybe (List (Tree a)))


trunk : Tree a -> a
trunk (Tree x _) =
    x


toListWith : (a -> b) -> Tree a -> List b
toListWith f tree =
    case tree of
        Tree x bs ->
            case bs of
                Just xs ->
                    f x :: List.concatMap (toListWith f) xs

                Nothing ->
                    [ f x ]


{-| Doesn't check for circularity. Backend takes care of that.
-}
fromListWith : (a -> b) -> (a -> Maybe (List a)) -> List a -> List (Tree b)
fromListWith f children list =
    let
        buildForest : List a -> List (Tree b) -> List (Tree b)
        buildForest xs acc =
            case xs of
                [] ->
                    acc

                y :: ys ->
                    Tree (f y) (Maybe.map (\x -> buildForest x []) (children y)) :: buildForest ys acc
    in
    buildForest list []



-- VIEW


view :
    Html msg
    -> (a -> String)
    -> (Tree a -> Html msg)
    -> List (Tree a)
    -> Html msg
view default key viewX forest =
    let
        content : Html msg
        content =
            if List.isEmpty forest then
                default

            else
                forest
                    |> List.map (\x -> ( (key << trunk) x, viewX x ))
                    |> Html.Keyed.ul [ Aria.role "group" ]
    in
    Html.div [ A.class "treeview", Aria.role "tree" ] [ content ]


viewGroup : List ( String, Html msg ) -> Html msg
viewGroup =
    Html.Keyed.ul [ Aria.role "group" ]


viewItem :
    Bool
    -> (a -> String)
    -> Html msg
    -> (Tree a -> Html msg)
    -> Maybe (List (Tree a))
    -> Html msg
viewItem open_ key summary viewBranch bs =
    Html.li [ Aria.role "treeitem" ]
        (case bs of
            Just ((_ :: _) as xs) ->
                -- We got a non-empty list
                [ Html.details [ open open_ ]
                    [ Html.summary [] [ summary ]
                    , viewGroup
                        (List.map (\x -> ( key (trunk x), viewBranch x )) xs)
                    ]
                ]

            _ ->
                -- We got Nothing or an empty list
                [ Html.div [ noChildrenClass ] [ summary ] ]
        )


noChildrenClass : Attribute msg
noChildrenClass =
    A.class "treeview__noChildren"


open : Bool -> Attribute msg
open =
    AE.boolProperty "open"
