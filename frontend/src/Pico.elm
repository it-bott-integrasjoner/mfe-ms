module Pico exposing (container, containerFluid, contrast, grid, outline, secondary)

import Html.Attributes exposing (class)


container =
    class "container"


containerFluid =
    class "container-fluid"


secondary =
    class "secondary"


contrast =
    class "contrast"


outline =
    class "outline"


grid =
    class "grid"
