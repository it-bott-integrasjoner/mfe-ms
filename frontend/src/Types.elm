module Types exposing (Instance)


type alias Instance =
    { id : String
    , name : String
    , logo : String
    }
