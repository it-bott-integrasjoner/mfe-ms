module Api.Date exposing
    ( Date
      -- This is for elm-format to keep exports on multiple lines
    , Range
    , compareRange
    , dates
    , decode
    , fromPosix
    , range
    , rangeFromSingle
    , show
    )

import Date
import Json.Decode as D
import Time


type Date
    = D Date.Date


{-| Date range where the first date is guaranteed to be less than or
equal to the last.
-}
type Range
    = Range Date Date


rangeFromSingle : Date -> Range
rangeFromSingle date =
    Range date date


range : Date -> Date -> Range
range from to =
    Range from to


dates : Range -> ( Date, Date )
dates (Range from to) =
    ( from, to )



{- Currently unused
   encode : Date -> E.Value
   encode =
       show >> E.string
-}


fromPosix : Time.Posix -> Date
fromPosix =
    D << Date.fromPosix Time.utc


compare : Date -> Date -> Order
compare (D a) (D b) =
    Date.compare a b


compareRange : Range -> Order
compareRange (Range a b) =
    compare a b


show : Date -> String
show (D d) =
    Date.toIsoString d


decode : D.Decoder Date
decode =
    D.string
        |> D.andThen
            (\str ->
                case Date.fromIsoString str of
                    Err deadEnd ->
                        D.fail deadEnd

                    Ok date ->
                        D.succeed (D date)
            )
