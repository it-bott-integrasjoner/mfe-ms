module Api.Form.GetManntall exposing (Handlers, OrganizationTree, optimizeSelected, view)

import Api exposing (DownloadProgress(..), Organization, Paths, getPaths)
import Api.Date as Date exposing (Date)
import Api.Form exposing (progressButton, viewCheckbox, viewInputDate)
import Api.GetManntall as Manntall
import Cldr.Format.DateTime
import Cldr.Locale
import Html exposing (Attribute, Html, button, div, fieldset, form, label, text)
import Html.Attributes as A
import Html.Events as E
import Iso8601
import Layout exposing (Size(..), icon)
import Lib exposing (showBool)
import Material.Icons.Round as I
import Pico
import Set exposing (Set)
import Time
import Tree exposing (Tree(..))


type alias OrganizationTree a =
    Tree { a | organization : Organization }


type alias Handlers msg =
    { onRecursiveToggled : msg
    , onMdateChanged : Maybe Date -> msg
    , onValgssluttChanged : Maybe Date -> msg
    , onToggleRootSelected : Organization -> msg
    , onSubmit : msg
    }


view :
    Paths
    -> { locale : Cldr.Locale.Locale, timeZone : Time.Zone }
    -> { organizations : List (OrganizationTree a), lastUpdate : Maybe Time.Posix }
    -> Handlers msg
    -> Manntall.Parameters
    -> DownloadProgress
    -> Html msg
view paths { locale, timeZone } { organizations, lastUpdate } handlers parameters downloadProgress =
    let
        isDownloading : Bool
        isDownloading =
            case downloadProgress of
                Downloading _ ->
                    True

                _ ->
                    False

        dates : Maybe Date.Range
        dates =
            Maybe.map2 Date.range parameters.mdate parameters.valgsslutt

        selectedOrganizations : Set Int
        selectedOrganizations =
            Set.fromList parameters.root

        recursiveDownload : Bool
        recursiveDownload =
            Maybe.withDefault True parameters.recursive

        downloadButton : Html msg
        downloadButton =
            case downloadProgress of
                Downloading p ->
                    progressButton "Laster ned" p

                _ ->
                    let
                        disabled : Bool
                        disabled =
                            dates == Nothing || (Set.isEmpty selectedOrganizations && not recursiveDownload)
                    in
                    button
                        [ A.type_ "submit"
                        , A.class "inline"
                        , A.disabled disabled
                        ]
                        [ icon Medium I.cloud_download, text " Last ned" ]

        recursiveDownloadCheckbox : Html msg
        recursiveDownloadCheckbox =
            label []
                [ viewCheckbox
                    recursiveDownload
                    (always (Just True))
                    []
                    (always handlers.onRecursiveToggled)
                    (Manntall.recursive (Just recursiveDownload))
                , text "Inkludér alle underenheter"
                ]

        fieldset_ : Html msg
        fieldset_ =
            fieldset [ A.disabled isDownloading ]
                [ viewDateFields dates handlers
                , viewOrganizationTree
                    { selectedOrganizations = selectedOrganizations
                    , recursiveDownload = recursiveDownload
                    }
                    handlers
                    organizations
                , div []
                    [ recursiveDownloadCheckbox ]
                ]

        lastUpdate_ : Html msg
        lastUpdate_ =
            Maybe.withDefault (text "Ukjent")
                << Maybe.map
                    (\x ->
                        Html.time
                            [ A.datetime << Iso8601.fromTime <| x ]
                            [ text << Cldr.Format.DateTime.format Cldr.Format.DateTime.medium locale timeZone <| x ]
                    )
            <|
                lastUpdate
    in
    form
        [ A.action ((getPaths paths).manntall Nothing)
        , E.onSubmit handlers.onSubmit
        ]
        [ Html.p [] [ text "Mellomlageret ble sist oppdatert ", lastUpdate_ ]
        , fieldset_
        , downloadButton
        ]


viewOrganizationTree :
    { m | recursiveDownload : Bool, selectedOrganizations : Set Int }
    -> Handlers msg
    -> List (OrganizationTree a)
    -> Html msg
viewOrganizationTree args handlers organizations =
    let
        selectedOrganizations : Set Int
        selectedOrganizations =
            if args.recursiveDownload then
                optimizeSelected args organizations
                    |> Set.fromList

            else
                args.selectedOrganizations

        viewOrganization : Bool -> OrganizationTree a -> Html msg
        viewOrganization implicitlySelected (Tree { organization } branches) =
            let
                selected : Bool
                selected =
                    Set.member organization.id selectedOrganizations

                viewBranch : OrganizationTree a -> Html msg
                viewBranch =
                    viewOrganization
                        (implicitlySelected || (selected && args.recursiveDownload))

                summary_ : Html msg
                summary_ =
                    label []
                        [ viewCheckbox (selected || implicitlySelected)
                            String.toInt
                            [ A.disabled implicitlySelected
                            ]
                            (always (handlers.onToggleRootSelected organization))
                            (Manntall.root organization.id)
                        , text organization.name
                        ]
            in
            Tree.viewItem
                -- Fakultet is level 3
                (organization.depth < 3)
                (String.fromInt << getId)
                summary_
                viewBranch
                branches

        getId : { a | organization : Organization } -> Int
        getId =
            .id << .organization
    in
    Tree.view
        (text "Fant ingen organisasjonsenheter")
        (String.fromInt << getId)
        (viewOrganization False)
        organizations


viewDateFields : Maybe Date.Range -> Handlers msg -> Html msg
viewDateFields dates handlers =
    let
        ( start, end ) =
            case dates of
                Just x ->
                    Tuple.mapBoth Just Just (Date.dates x)

                Nothing ->
                    ( Nothing, Nothing )

        invalidAttribute : Attribute msg
        invalidAttribute =
            (A.attribute "aria-invalid" << showBool) (dates == Nothing || (Maybe.map Date.compareRange dates == Just GT))

        viewDateField : List (Attribute msg) -> String -> (Maybe Date -> Manntall.Parameter Date) -> (Maybe Date -> msg) -> Maybe Date -> Html msg
        viewDateField attributes label_ mkParam tagger x =
            label []
                [ text label_
                , viewInputDate (invalidAttribute :: A.required True :: attributes) tagger (mkParam x)
                ]

        startDateField : Html msg
        startDateField =
            viewDateField
                []
                "Startdato"
                Manntall.mdate
                handlers.onMdateChanged
                start

        endDateField : Html msg
        endDateField =
            viewDateField
                []
                "Sluttdato"
                Manntall.valgsslutt
                handlers.onValgssluttChanged
                end
    in
    div [ Pico.grid ] [ startDateField, endDateField ]


{-| Remove elements from `organizations` if it has a selected ancestor
-}
optimizeSelected :
    { m | recursiveDownload : Bool, selectedOrganizations : Set Int }
    -> List (OrganizationTree a)
    -> List Int
optimizeSelected { recursiveDownload, selectedOrganizations } organizations =
    let
        isSelected : { a | organization : { b | id : Int } } -> Bool
        isSelected x =
            Set.member x.organization.id selectedOrganizations

        go : List (OrganizationTree a) -> List Int -> List Int
        go xs acc =
            case xs of
                [] ->
                    acc

                (Tree t bs) :: ys ->
                    if isSelected t then
                        -- Keep and visit siblings
                        go ys (t.organization.id :: acc)

                    else
                        go
                            -- Visit siblings
                            ys
                            -- Visit branches
                            (go (Maybe.withDefault [] bs) acc)
    in
    if not recursiveDownload || Set.isEmpty selectedOrganizations then
        Set.toList selectedOrganizations

    else
        go organizations []
