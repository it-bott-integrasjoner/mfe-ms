module CustomScalarCodecs exposing (DateTime, codecs)

import Api.Scalar
import Iso8601
import Time


type alias DateTime =
    Time.Posix


codecs : Api.Scalar.Codecs Time.Posix
codecs =
    Api.Scalar.defineCodecs
        { codecDateTime =
            { encoder = Iso8601.encode
            , decoder = Iso8601.decoder
            }
        }
