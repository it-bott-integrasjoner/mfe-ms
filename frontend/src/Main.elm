module Main exposing
    ( Flags
    , GraphqlData
    , Model
    , Msg
    , OrganizationTree
    , OrganizationTreeT
    , User
    , WebData
    , main
    )

import Api
    exposing
        ( DownloadProgress(..)
        , Organization
        , Paths
        , getCurrentUser
        , getGraphqlData
        , getPaths
        , httpErrorToString
        , mkPaths
        )
import Api.Date
import Api.Form.GetManntall as GetManntallForm
import Api.GetManntall as Manntall
import Browser
import Browser.Events exposing (Visibility(..), onVisibilityChange)
import Browser.Navigation as Nav
import Cldr.Locale
import File.Download
import Graphql.Http
import Html
    exposing
        ( Html
        , a
        , article
        , aside
        , button
        , div
        , h2
        , li
        , p
        , section
        , text
        , ul
        )
import Html.Attributes as A
import Html.Attributes.Aria as Aria
import Html.Events exposing (onClick)
import Html.Lazy as H
import Http
import Json.Decode
import Layout exposing (Layout, Size(..), Toast, icon, toastyConfig)
import Lib exposing (ariaBusy)
import Material.Icons.Round as I
import Page.NotFound
import Pico exposing (secondary)
import RemoteData exposing (RemoteData(..))
import Route exposing (BasePath, Page(..), Route, getBasePath)
import Set exposing (Set)
import Task
import Time
import Toasty
import Toasty.Defaults as Toasty
import Tree exposing (Tree)
import Tuple
import Types exposing (Instance)
import Url



-- MAIN


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL


type alias WebData a =
    RemoteData (Graphql.Http.Error ()) a


type User
    = LoggedIn Api.User
    | LoggingOut


type alias GraphqlData =
    { organizations : List OrganizationTree, lastUpdate : Maybe Time.Posix }


type alias Model =
    { basePath : BasePath
    , apiPath : Paths
    , key : Nav.Key
    , route : Route
    , user : WebData User
    , toasties : Toasty.Stack Toast
    , instance : Maybe Instance
    , recursiveDownload : Bool
    , graphqlData : WebData GraphqlData
    , dates : Maybe Api.Date.Range
    , selectedOrganizations : Set Int
    , manntallDownloadProgress : DownloadProgress
    , locale : Cldr.Locale.Locale
    , timeZone : Time.Zone
    }


type Date
    = StartDate Api.Date.Date
    | EndDate Api.Date.Date


type alias OrganizationTreeT =
    { organization : Organization }


type alias OrganizationTree =
    Tree OrganizationTreeT


manntallFilename : String
manntallFilename =
    "manntall.json"


convertGraphqlData : Api.GraphqlData -> GraphqlData
convertGraphqlData data =
    let
        children : Api.Organization -> Maybe (List Api.Organization)
        children organization =
            List.filter (\x -> x.parentId == Just organization.id) data.organizations
                |> Just
    in
    { organizations = Tree.fromListWith (\x -> { organization = x }) children (List.filter (\x -> x.parentId == Nothing) data.organizations)
    , lastUpdate = data.lastUpdate
    }


type alias Flags =
    { basePath : String
    , apiPath : String
    , loginPath : String
    , logoutPath : String
    , instance : String
    }


requiredScope : String
requiredScope =
    "authorized"


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init { basePath, apiPath, loginPath, logoutPath, instance } url key =
    let
        base : BasePath
        base =
            Route.mkBasePath basePath

        ins : Maybe Instance
        ins =
            case instance of
                "uib" ->
                    Just
                        { id = instance
                        , name = "Universitetet i Bergen"
                        , logo = getBasePath base ++ "assets/logo_" ++ instance ++ ".svg"
                        }

                "uio" ->
                    Just
                        { id = instance
                        , name = "Universitetet i Oslo"
                        , logo = getBasePath base ++ "assets/logo_" ++ instance ++ ".svg"
                        }

                _ ->
                    Nothing

        api : Paths
        api =
            mkPaths { base = apiPath, login = loginPath, logout = logoutPath, home = getBasePath base }
    in
    ( { basePath = base
      , apiPath = api
      , key = key
      , route = Route.fromUrl base url
      , user = Loading
      , toasties = Toasty.initialState
      , instance = ins
      , recursiveDownload = True
      , graphqlData = NotAsked
      , dates = Nothing
      , selectedOrganizations = Set.empty
      , manntallDownloadProgress = NotDownloading
      , locale = Cldr.Locale.nb
      , timeZone = Time.utc
      }
    , Cmd.batch
        [ getCurrentUser api GotUser
        , Task.perform (GotDate << Just << StartDate << Api.Date.fromPosix) Time.now
        , Task.perform GotTimeZone Time.here
        ]
    )



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | GotUser (WebData Api.User)
    | GotUnauthenticated
    | ToastyMsg (Toasty.Msg Toast)
    | GotGraphqlData (WebData Api.GraphqlData)
    | GetGraphqlDataClicked
    | VisibilityChanged Visibility
    | GotDate (Maybe Date)
    | ToggleSelected Organization
    | RecursiveDownloadToggled
    | DownloadFormSubmitted
    | GotManntall (Result Api.Error String)
    | GotManntallProgress Http.Progress
    | GotTimeZone Time.Zone


handlers : GetManntallForm.Handlers Msg
handlers =
    { onRecursiveToggled = RecursiveDownloadToggled
    , onMdateChanged = GotDate << Maybe.map StartDate
    , onValgssluttChanged = GotDate << Maybe.map EndDate
    , onToggleRootSelected = ToggleSelected
    , onSubmit = DownloadFormSubmitted
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        getGraphqlData_ : Cmd Msg
        getGraphqlData_ =
            getGraphqlData model.apiPath GotGraphqlData
    in
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    if isLogoutRequest model.apiPath urlRequest then
                        ( { model | graphqlData = NotAsked, user = Success LoggingOut }, Nav.load (Url.toString url) )

                    else if isLoginRequest model.apiPath urlRequest then
                        ( { model | user = Loading }, Nav.load (Url.toString url) )

                    else if isFrontendRequest model.basePath urlRequest then
                        ( model, Nav.pushUrl model.key (Url.toString url) )

                    else
                        ( model, Nav.load (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | route = Route.fromUrl model.basePath url }
            , Cmd.none
            )

        ToastyMsg subMsg ->
            Toasty.update toastyConfig ToastyMsg subMsg model

        GotUser data ->
            case RemoteData.map LoggedIn data of
                (Success (LoggedIn u)) as user ->
                    let
                        res : ( Model, Cmd Msg )
                        res =
                            if RemoteData.isSuccess model.graphqlData then
                                ( { model | user = user }, Cmd.none )

                            else
                                ( { model | user = user, graphqlData = Loading }, getGraphqlData_ )
                    in
                    if user /= model.user then
                        res |> Toasty.addToast toastyConfig ToastyMsg (Toasty.Success "Innlogget" ("Du er innlogget som " ++ u.name))

                    else
                        res

                (Failure err) as user ->
                    ( { model | user = user }, Cmd.none )
                        |> addToastFromGraphqlError err

                user ->
                    ( { model | user = user }, Cmd.none )

        GotUnauthenticated ->
            ( { model | graphqlData = NotAsked, user = NotAsked, toasties = Toasty.initialState }, Cmd.none )
                |> (case model.user of
                        Success _ ->
                            addPersistentToast 0 (Toasty.Warning "Sesjon utløpt" "Du er nå blitt logget ut")

                        _ ->
                            identity
                   )

        GotGraphqlData (Failure err) ->
            ( { model | graphqlData = Failure err }, Cmd.none )
                |> addToastFromGraphqlError err

        GotGraphqlData (Success data) ->
            let
                newData : GraphqlData
                newData =
                    convertGraphqlData data

                selectedOrgs : Set Int
                selectedOrgs =
                    -- Keep selections
                    if Set.isEmpty model.selectedOrganizations then
                        Set.empty

                    else
                        let
                            orgIds : Set Int
                            orgIds =
                                List.concatMap (Tree.toListWith (.id << .organization)) newData.organizations
                                    |> Set.fromList
                        in
                        -- Make sure we remove org ids not in the new tree
                        Set.intersect orgIds model.selectedOrganizations
            in
            ( { model | graphqlData = Success newData, selectedOrganizations = selectedOrgs }, Cmd.none )

        GotGraphqlData data ->
            ( { model | graphqlData = RemoteData.map convertGraphqlData data }, Cmd.none )

        GetGraphqlDataClicked ->
            ( { model | graphqlData = Loading }, getGraphqlData_ )

        VisibilityChanged Visible ->
            -- Fetch user to check if we are still logged in
            ( model, getCurrentUser model.apiPath GotUser )

        VisibilityChanged Hidden ->
            ( model, Cmd.none )

        ToggleSelected t ->
            let
                selected : Set Int
                selected =
                    (if Set.member t.id model.selectedOrganizations then
                        Set.remove

                     else
                        Set.insert
                    )
                        t.id
                        model.selectedOrganizations
            in
            ( { model | selectedOrganizations = selected }, Cmd.none )

        GotDate maybeDate ->
            ( { model
                | dates =
                    Maybe.map
                        (\date -> updateDateRange date model.dates)
                        maybeDate
              }
            , Cmd.none
            )

        RecursiveDownloadToggled ->
            ( { model | recursiveDownload = not model.recursiveDownload }, Cmd.none )

        DownloadFormSubmitted ->
            case model.graphqlData of
                Success data ->
                    case model.dates of
                        Just range ->
                            let
                                ( start, end ) =
                                    Api.Date.dates range
                            in
                            let
                                selected : List Int
                                selected =
                                    GetManntallForm.optimizeSelected model data.organizations

                                getManntall : Cmd Msg
                                getManntall =
                                    Api.getManntall model.apiPath parameters GotManntall (Just manntallFilename)

                                parameters : Maybe Manntall.Parameters
                                parameters =
                                    Just
                                        { recursive = Just model.recursiveDownload
                                        , mdate = Just start
                                        , valgsslutt = Just end
                                        , root = selected
                                        }
                            in
                            ( { model
                                | manntallDownloadProgress =
                                    Downloading
                                        { received = 0
                                        , size = Nothing
                                        }
                              }
                            , getManntall
                            )

                        _ ->
                            ( model, Cmd.none )
                                |> addPersistentToast 0 (Toasty.Error "Feil i skjema" "Du må velge datoer")

                Failure err ->
                    ( model, Cmd.none )
                        |> addToastFromGraphqlError err

                _ ->
                    ( model, Cmd.none )
                        |> addPersistentToast 0 (Toasty.Warning "Mangler data" "Har ikke lastet organisasjonstreet ennå")

        GotManntall result ->
            let
                updatedModel : Model
                updatedModel =
                    { model | manntallDownloadProgress = DoneDownloading }
            in
            case result of
                Ok content ->
                    ( updatedModel, File.Download.string manntallFilename "application/json" content )

                Err err ->
                    ( updatedModel, Cmd.none )
                        |> addToastFromApiError err

        GotManntallProgress progress ->
            case progress of
                Http.Receiving x ->
                    ( { model | manntallDownloadProgress = Downloading x }, Cmd.none )

                Http.Sending _ ->
                    ( model, Cmd.none )

        GotTimeZone zone ->
            ( { model | timeZone = zone }, Cmd.none )


updateDateRange : Date -> Maybe Api.Date.Range -> Api.Date.Range
updateDateRange new maybeOld =
    case maybeOld of
        Just old ->
            let
                ( oldStart, oldEnd ) =
                    Api.Date.dates old
            in
            case new of
                StartDate newStart ->
                    Api.Date.range newStart oldEnd

                EndDate newEnd ->
                    Api.Date.range oldStart newEnd

        Nothing ->
            case new of
                StartDate newStart ->
                    Api.Date.rangeFromSingle newStart

                EndDate newEnd ->
                    Api.Date.rangeFromSingle newEnd


isUnauthenticated : Graphql.Http.Error a -> Bool
isUnauthenticated error =
    case error of
        Graphql.Http.HttpError (Graphql.Http.BadStatus metadata _) ->
            metadata.statusCode == 401

        _ ->
            False


toSimpleHttpError : Graphql.Http.HttpError -> Http.Error
toSimpleHttpError httpError =
    case httpError of
        Graphql.Http.BadUrl url ->
            Http.BadUrl url

        Graphql.Http.Timeout ->
            Http.Timeout

        Graphql.Http.NetworkError ->
            Http.NetworkError

        Graphql.Http.BadStatus metadata _ ->
            Http.BadStatus metadata.statusCode

        Graphql.Http.BadPayload jsonError ->
            Http.BadBody (Json.Decode.errorToString jsonError)


addToastFromGraphqlError : Graphql.Http.Error a -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
addToastFromGraphqlError error ( model, cmd ) =
    case error of
        Graphql.Http.HttpError err ->
            addToastFromHttpError (toSimpleHttpError err) ( model, cmd )

        Graphql.Http.GraphqlError _ _ ->
            ( model, cmd )


addToastFromApiError : Api.Error -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
addToastFromApiError error ( model, cmd ) =
    case error of
        Api.UnknownError err ->
            addToastFromHttpError err ( model, cmd )

        Api.Error err ->
            let
                f : a -> ( Model, Cmd Msg )
                f _ =
                    if err.status == 401 then
                        update GotUnauthenticated model

                    else
                        addPersistentToast 0 (Toasty.Error ("HTTP feil (" ++ String.fromInt err.status ++ ")") err.detail) ( model, cmd )
            in
            f ( model, cmd )


addToastFromHttpError : Http.Error -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
addToastFromHttpError error ( model, cmd ) =
    let
        statusCode : Maybe Int
        statusCode =
            case error of
                Http.BadStatus x ->
                    Just x

                _ ->
                    Nothing
    in
    if statusCode == Just 401 then
        -- Log out user if we get a 401 response
        update GotUnauthenticated model

    else
        let
            trunc : Int -> String -> String
            trunc limit x =
                if String.length x > limit then
                    String.left limit x ++ "…"

                else
                    x
        in
        addPersistentToast
            0
            (Toasty.Error "HTTP feil" (httpErrorToString error |> trunc 2048))
            ( model, cmd )


addPersistentToast : Float -> Toast -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
addPersistentToast delay =
    Toasty.addPersistentToast (Toasty.delay delay toastyConfig) ToastyMsg



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ onVisibilityChange VisibilityChanged
        , Http.track manntallFilename GotManntallProgress
        ]



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        mkLayout : Html msg -> Layout msg
        mkLayout body =
            { title = Nothing
            , paths = model.apiPath
            , attributes = []
            , body = [ warning, body ]
            , instance = model.instance
            }

        warning : Html msg
        warning =
            aside []
                [ article []
                    [ icon Large I.warning
                    , text <|
                        "Viktig: For å ivareta sikkerheten skal nedlastede filer slettes etter"
                            ++ " behandling, og filer skal ikke lastes ned til privat datamaskin."
                    ]
                ]

        loginButton : a -> Html msg
        loginButton _ =
            section [] [ text "Du må ", a [ A.href loginPath ] [ text "logge inn" ], text " for å fortsette" ]

        view_ : Layout Msg
        view_ =
            case model.route.page of
                NotFound ->
                    Page.NotFound.view { paths = model.apiPath, instance = model.instance }

                Home ->
                    let
                        simpleBusy : String -> Layout msg
                        simpleBusy text_ =
                            let
                                layout : Layout msg
                                layout =
                                    mkLayout (text text_)
                            in
                            { layout | attributes = ariaBusy True :: layout.attributes }
                    in
                    case model.user of
                        Success LoggingOut ->
                            simpleBusy "Logger ut"

                        Success (LoggedIn user) ->
                            let
                                authorized : Bool
                                authorized =
                                    List.member requiredScope user.scopes
                            in
                            if authorized then
                                H.lazy viewGraphqlData model
                                    |> mkLayout

                            else
                                mkLayout (text "Du har ikke tilgang til denne tjenesten")

                        Loading ->
                            simpleBusy "Henter bruker"

                        NotAsked ->
                            mkLayout (loginButton ())

                        Failure failure ->
                            if isUnauthenticated failure then
                                mkLayout (loginButton ())

                            else
                                section []
                                    [ viewGraphqlErrors failure, p [] [ text "Dette er sannsynligvis en intern feil" ] ]
                                    |> mkLayout

        loginPath : String
        loginPath =
            (getPaths model.apiPath).login

        sessionButton : Html msg
        sessionButton =
            case model.user of
                Success _ ->
                    a
                        [ secondary, A.href (getPaths model.apiPath).logout, Aria.ariaLabel "Logg ut", Aria.role "button" ]
                        [ icon Large I.logout ]

                _ ->
                    a
                        [ A.href loginPath, Aria.ariaLabel "Logg inn", Aria.role "button" ]
                        [ icon Large I.login ]
    in
    Layout.view view_ sessionButton ToastyMsg model.toasties


toManntallParameters : Model -> Manntall.Parameters
toManntallParameters model =
    let
        ( start, end ) =
            case model.dates of
                Just x ->
                    Tuple.mapBoth Just Just (Api.Date.dates x)

                Nothing ->
                    ( Nothing, Nothing )
    in
    { mdate = start
    , valgsslutt = end
    , recursive = Just model.recursiveDownload
    , root = Set.toList model.selectedOrganizations
    }


viewGraphqlData : Model -> Html Msg
viewGraphqlData model =
    case model.graphqlData of
        Success data ->
            GetManntallForm.view model.apiPath
                { locale = model.locale, timeZone = model.timeZone }
                data
                handlers
                (toManntallParameters model)
                model.manntallDownloadProgress

        NotAsked ->
            button [ A.class "inline", onClick GetGraphqlDataClicked ] [ text "Hent data" ]

        Loading ->
            button [ A.class "inline", ariaBusy True, A.disabled True ] [ text "Henter data" ]

        Failure failure ->
            div []
                [ div [] [ viewGraphqlErrors failure ]
                , a [ Aria.role "button", A.href "#", onClick GetGraphqlDataClicked ] [ text "Prøv igjen" ]
                ]


viewGraphqlErrors : Graphql.Http.Error a -> Html msg
viewGraphqlErrors error =
    let
        heading : String -> Html msg
        heading msg =
            h2 [] [ icon Large I.error, text " ", text msg ]

        content : String -> List (Html msg) -> Html msg
        content headingText html =
            section [] [ heading headingText, div [] html ]
    in
    case error of
        Graphql.Http.GraphqlError _ errs ->
            -- Graphql error
            let
                message : List (Html msg)
                message =
                    case errs of
                        [ err ] ->
                            [ text <| err.message ]

                        _ ->
                            [ ul [] <| List.map (\err -> li [] [ text err.message ]) errs ]
            in
            content "GraphQL feil" message

        Graphql.Http.HttpError err ->
            content "HTTP feil" [ text <| (httpErrorToString << toSimpleHttpError) err ]



-- HELPERS


isFrontendRequest : BasePath -> Browser.UrlRequest -> Bool
isFrontendRequest base urlRequest =
    case urlRequest of
        Browser.Internal url ->
            String.startsWith (getBasePath base) url.path

        Browser.External _ ->
            False


isLogoutRequest : Paths -> Browser.UrlRequest -> Bool
isLogoutRequest paths urlRequest =
    case urlRequest of
        Browser.Internal url ->
            String.startsWith (getPaths paths).logout url.path

        Browser.External _ ->
            False


isLoginRequest : Paths -> Browser.UrlRequest -> Bool
isLoginRequest paths urlRequest =
    case urlRequest of
        Browser.Internal url ->
            String.startsWith (getPaths paths).login url.path

        Browser.External _ ->
            False
