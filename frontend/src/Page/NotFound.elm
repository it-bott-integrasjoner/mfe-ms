module Page.NotFound exposing (view)

import Api exposing (Paths, getPaths)
import Html exposing (a, h1, section, text)
import Html.Attributes exposing (href)
import Layout exposing (Layout)
import Route exposing (Page(..), mkBasePath)
import Types exposing (Instance)


view : { paths : Paths, instance : Maybe Instance } -> Layout msg
view { paths, instance } =
    { title = Just "Not found"
    , attributes = []
    , paths = paths
    , body =
        [ section []
            [ h1 [] [ text "Fant ikke side" ]
            , a [ href (Route.toUrl { page = Home, base = mkBasePath (getPaths paths).home }) ] [ text "Gå tilbake" ]
            ]
        ]
    , instance = instance
    }
