module Layout exposing (Layout, Size(..), Toast, icon, toastyConfig, view)

import Api exposing (Paths, getPaths)
import Browser
import Html exposing (Attribute, Html, a, aside, div, img, li, main_, nav, span, text, ul)
import Html.Attributes exposing (alt, class, height, href, src, width)
import Html.Attributes.Aria exposing (ariaLabel)
import Material.Icons.Types exposing (Coloring(..), Icon)
import Pico
import Toasty
import Toasty.Defaults
import Types exposing (Instance)


type alias Toast =
    Toasty.Defaults.Toast


type alias Layout msg =
    { title : Maybe String
    , paths : Paths
    , attributes : List (Attribute msg)
    , body : List (Html msg)
    , instance : Maybe Instance
    }


view : Layout msg -> Html msg -> (Toasty.Msg Toast -> msg) -> Toasty.Stack Toast -> Browser.Document msg
view page sessionButtonView msg toasties =
    let
        body : List (Html msg)
        body =
            [ div [ Pico.container ]
                [ navView page "Manntallsrapport" sessionButtonView
                , main_ [] [ div page.attributes page.body ]
                , aside [ class "toasty" ] [ Toasty.view toastyConfig Toasty.Defaults.view msg toasties ]
                ]
            ]
    in
    { title = Maybe.withDefault "Manntallsrapport for Evalg" page.title
    , body = body
    }


navView : Layout msg -> String -> Html msg -> Html msg
navView { paths, instance } heading sessionButtonView =
    let
        paths_ : Api.PathT
        paths_ =
            getPaths paths

        logo : List (Html msg)
        logo =
            case instance of
                Just x ->
                    [ li [] [ img [ alt x.name, width 200, height 200, src x.logo, class "logo" ] [] ] ]

                _ ->
                    []
    in
    nav [ Pico.containerFluid ]
        [ ul []
            (logo
                ++ [ li []
                        [ a [ href paths_.home, ariaLabel "Hjem", class "secondary" ]
                            [ span [] [ text heading ] ]
                        ]
                   , li []
                        [ a [ href paths_.apiDocs ] [ text "API dokumentasjon" ] ]
                   ]
            )
        , ul []
            [ li []
                [ sessionButtonView ]
            ]
        ]


type Size
    = Medium
    | Large


icon : Size -> Icon msg -> Html msg
icon size ico =
    let
        s : Int
        s =
            case size of
                Medium ->
                    24

                Large ->
                    32
    in
    span [ class "icon" ] [ ico s Inherit ]



-- Toasty


toastyConfig : Toasty.Config msg
toastyConfig =
    Toasty.Defaults.config
