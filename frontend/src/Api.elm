module Api exposing
    ( ApiError
    , DownloadProgress(..)
    , Error(..)
    , GraphqlData
    , Organization
    , PathT
    , Paths
    , User
    , getCurrentUser
    , getGraphqlData
    , getManntall
    , getPaths
    , httpErrorToString
    , mkPaths
    )

import Api.GetManntall as Manntall
import Api.Object
import Api.Object.CacheInfo as ApiCacheInfo
import Api.Object.Organization as ApiOrganization
import Api.Object.User as ApiUser
import Api.Query as ApiQuery
import CustomScalarCodecs exposing (DateTime)
import Graphql.Http
import Graphql.Operation exposing (RootQuery)
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet)
import Http
import Json.Decode as D
import RemoteData exposing (RemoteData)


type DownloadProgress
    = NotDownloading
    | Downloading { received : Int, size : Maybe Int }
    | DoneDownloading


type alias User =
    { name : String
    , issuedAt : DateTime
    , expiresAt : DateTime
    , scopes : List String
    }


type alias Organization =
    { id : Int
    , shortName : String
    , name : String
    , parentId : Maybe Int
    , depth : Int
    }


type alias GraphqlData =
    { organizations : List Organization
    , lastUpdate : Maybe DateTime
    }


type alias PathT =
    { manntall : Maybe Manntall.Parameters -> String
    , graphql : String
    , login : String
    , logout : String
    , apiDocs : String
    , home : String
    }


type Paths
    = ApiPaths PathT


mkPaths : { a | base : String, login : String, logout : String, home : String } -> Paths
mkPaths paths =
    let
        base : String
        base =
            if String.endsWith "/" paths.base then
                String.dropRight 1 paths.base

            else
                paths.base

        manntall_ : Maybe Manntall.Parameters -> String
        manntall_ params =
            case params of
                Just x ->
                    (String.append base << Manntall.uri) x

                Nothing ->
                    base ++ Manntall.path
    in
    ApiPaths
        { manntall = manntall_
        , graphql = base ++ "/graphql/"
        , login = paths.login
        , logout = paths.logout
        , apiDocs = base ++ "/docs"
        , home = paths.home
        }


getPaths : Paths -> PathT
getPaths (ApiPaths paths) =
    paths


currentUserSelection : SelectionSet User Api.Object.User
currentUserSelection =
    SelectionSet.map4 User
        ApiUser.name
        ApiUser.issuedAt
        ApiUser.expiresAt
        ApiUser.scopes


currentUserQuery : SelectionSet User RootQuery
currentUserQuery =
    ApiQuery.me currentUserSelection


getCurrentUser : Paths -> (RemoteData (Graphql.Http.Error a) User -> c) -> Cmd c
getCurrentUser (ApiPaths { graphql }) toMsg =
    currentUserQuery
        |> Graphql.Http.queryRequest graphql
        |> Graphql.Http.send
            (Graphql.Http.discardParsedErrorData >> RemoteData.fromResult >> toMsg)


lastUpdateSelection : SelectionSet (Maybe DateTime) Api.Object.CacheInfo
lastUpdateSelection =
    ApiCacheInfo.lastUpdate


lastUpdateQuery : SelectionSet (Maybe DateTime) RootQuery
lastUpdateQuery =
    ApiQuery.cache lastUpdateSelection


httpErrorToString : Http.Error -> String
httpErrorToString error =
    case error of
        Http.BadUrl url ->
            "Invalid URL: " ++ url

        Http.Timeout ->
            "Unable to reach the server, try again"

        Http.NetworkError ->
            "Unable to reach the server, check your network connection"

        Http.BadStatus n ->
            if n >= 400 && n < 500 then
                "Client error: status code " ++ String.fromInt n

            else if n >= 500 && n < 600 then
                "Server error: status code " ++ String.fromInt n

            else
                "Unknown error: status code " ++ String.fromInt n

        Http.BadBody errorMessage ->
            errorMessage


organizationTreeSelection : SelectionSet Organization Api.Object.Organization
organizationTreeSelection =
    SelectionSet.map5 Organization
        ApiOrganization.id
        ApiOrganization.shortName
        ApiOrganization.name
        ApiOrganization.parentId
        ApiOrganization.depth


organizationsQuery : SelectionSet (List Organization) RootQuery
organizationsQuery =
    ApiQuery.organizations organizationTreeSelection


graphqlDataQuery : SelectionSet GraphqlData RootQuery
graphqlDataQuery =
    SelectionSet.map2 GraphqlData
        organizationsQuery
        lastUpdateQuery


getGraphqlData : Paths -> (RemoteData (Graphql.Http.Error a) GraphqlData -> c) -> Cmd c
getGraphqlData (ApiPaths { graphql }) toMsg =
    graphqlDataQuery
        |> Graphql.Http.queryRequest graphql
        |> Graphql.Http.send (Graphql.Http.discardParsedErrorData >> RemoteData.fromResult >> toMsg)



-- Backend REST API


type Error
    = Error ApiError
    | UnknownError Http.Error


{-| class mfe\_ms.backend.api.Error
-}
type alias ApiError =
    { status : Int
    , detail : String
    }


apiErrorDecoder : String -> D.Decoder ApiError
apiErrorDecoder defaultDetails =
    let
        string : D.Decoder String
        string =
            D.map (Maybe.withDefault "") (D.maybe D.string)
    in
    D.map2 ApiError
        (D.field "status" D.int)
        (D.oneOf [ D.field "detail" string, D.succeed defaultDetails ])


convertResponse : Http.Response String -> Result Error String
convertResponse response =
    let
        unknown : Http.Error -> Result Error value
        unknown =
            Err << UnknownError
    in
    case response of
        Http.GoodStatus_ _ x ->
            Ok x

        Http.BadUrl_ x ->
            (unknown << Http.BadUrl) x

        Http.Timeout_ ->
            unknown Http.Timeout

        Http.NetworkError_ ->
            unknown Http.NetworkError

        Http.BadStatus_ metadata body ->
            case D.decodeString (apiErrorDecoder metadata.statusText) body of
                Ok x ->
                    Err (Error x)

                _ ->
                    Err (Error { status = metadata.statusCode, detail = body })


getManntall :
    Paths
    -> Maybe Manntall.Parameters
    -> (Result Error String -> msg)
    -> Maybe String
    -> Cmd msg
getManntall paths parameters toMsg tracker =
    Http.request
        { method = "GET"
        , url = (.manntall << getPaths) paths parameters
        , expect = Http.expectStringResponse toMsg convertResponse
        , headers = []
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = tracker
        }
