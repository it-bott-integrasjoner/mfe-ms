import { Elm } from "../src/Main.elm";

document.addEventListener("DOMContentLoaded", function () {
  /* Workaround for https://github.com/parcel-bundler/parcel/issues/1186
   * Fetch flags here instead of using <script src="..." /> in html
   */
  fetch("../flags.json")
    .then(response => {
      response.json().then(flags => Elm.Main.init({flags}))
    })
    .catch(err => {
        document.body.innerHTML = err.message;
    })
});
