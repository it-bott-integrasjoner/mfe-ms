# https://docs.gunicorn.org/en/stable/configure.html

import mfe_ms.config as _mfe_ms_config

_mfe_ms_loader = _mfe_ms_config.get_config_loader()

wsgi_app = "mfe_ms.backend.asgi:create_app()"

if _mfe_ms_loader.logging:
    logconfig_dict = _mfe_ms_loader.logging

worker_class = "mfe_ms.backend.workers.BehindProxyWorker"
