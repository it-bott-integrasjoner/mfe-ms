ARG PYTHON_VERSION=3.11
# `python-base` sets up all our shared environment variables
FROM harbor.uio.no/mirrors/docker.io/library/python:${PYTHON_VERSION}-slim as python-base

# python
ENV PYTHONUNBUFFERED=1 \
    # prevents python creating .pyc files
    PYTHONDONTWRITEBYTECODE=1 \
    \
    # paths
    # this is where our requirements + virtual environment will live
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

# prepend poetry and venv to path
ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${PATH}"

# `builder-base` stage is used to build deps + create our virtual environment
FROM docker.io/python:${PYTHON_VERSION} as builder-base
# python
ENV PYTHONUNBUFFERED=1 \
    # prevents python creating .pyc files
    PYTHONDONTWRITEBYTECODE=1 \
    \
    # paths
    # this is where our requirements + virtual environment will live
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -y --no-install-recommends git

ARG POETRY_VERSION=1.7.0

ENV PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    \
    # poetry
    # https://python-poetry.org/docs/configuration/#using-environment-variables
    POETRY_VERSION=${POETRY_VERSION} \
    # make poetry install to this location
    POETRY_HOME="/opt/poetry" \
    # make poetry create the virtual environment in the project's root
    # it gets named `.venv`
    POETRY_VIRTUALENVS_IN_PROJECT=true

# Use pipx instead of pip
RUN python3 -m pip install --no-cache-dir --user pipx==1.1.0 \
    && python3 -m pipx ensurepath

ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${PATH}"

WORKDIR ${PYSETUP_PATH}

RUN /root/.local/bin/pipx install poetry=="${POETRY_VERSION}"

COPY poetry.lock pyproject.toml ./

RUN /root/.local/bin/poetry install --no-dev --no-interaction --no-ansi

# Create GraphQL schema
FROM python-base as code-generator

COPY --from=builder-base ${VENV_PATH} ${VENV_PATH}
COPY mfe_ms mfe_ms
COPY config.example.yaml config.yaml

RUN mkdir -p frontend/dist
RUN    python -m mfe_ms.backend.elm --output-directory=generated/api
RUN    python -m mfe_ms.backend.routes.graphql >generated/schema.graphql

# Build frontend
# Use same tag as in .gitlab-ci.yml
FROM node:20-alpine as frontend-builder

WORKDIR /home/node/frontend

COPY frontend .
COPY --from=code-generator generated/ generated/

RUN npm ci
RUN npm run build

# 'production' stage uses the clean 'python-base' stage and copies
# in only our runtime deps that were installed in the 'builder-base'
FROM python-base as production

LABEL no.uio.contact=bott-int-drift@usit.uio.no

COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]

WORKDIR /mfe-ms

COPY --from=builder-base ${VENV_PATH} ${VENV_PATH}
COPY --from=frontend-builder /home/node/frontend/dist frontend/dist

COPY . .

# Compile installed code
RUN python -m compileall -r 10 . \
    && install -v -d -m7700 /var/log/mfe-ms \
    && chmod +x /entrypoint.sh
