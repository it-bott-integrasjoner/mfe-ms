# Manntallsrapport for Evalg

## Dataporten

### Opprett applikasjon i [Dataporten](https://dashboard.dataporten.no)

Scopes:

- openid
- profile
- userid-feide
- groups-org
- groups-other
- groups-edu
- userinfo-entitlement

Use of entitlement:

Scope: `userinfo-entitlement` is not enabled by default.

To be able to choose this scope in dash board, we need to contact kontakt@uninett.no.
Entitlement values which comes from dataporten are sat in LDAP.

[For mor info: Extended userinfo](https://docs.feide.no/service_providers/openid_connect/oidc_authentication.html#extended-userinfo-from-feide-directories)

#### User types

Following user types can be configured using `entitlements`:

- `Authorized user`: Logged inn user with out any granted access
- `Organization admin`: Users who have access to download manntallsrapport within their organization.
- `Super user`: Users who have all granted all access

Configuration:

To use user type: `Organization admin` there is need to:

1. Set config: `auth.allowed_org_user_entitlement_prefix`
2. Update entielment for the user with format:

   Format: <auth.allowed_org_user_entitlement_prefix>:orgenhet.kort_navn

Example: Entitlement for the user <auth.allowed_org_user_entitlement_prefix>:hf,
gives access to download for the hf only
Note: Only use kort_navn from top three level of organization tree.  
Redirect URIs:

- http://localhost:4000/auth/dataporten/callback

Extended info -> Post logout redirect URI:

- http://localhost:4000/home/

## Elm

Frontend er skrevet i [elm](https://elm-lang.org/). Det er viktig at
man ikke bruker en global versjon av `elm` når man kompilerer prosjektet
og installerer pakker. Det kan føre til krøll. Bruk [npx](https://www.npmjs.com/package/npx)
til å kjøre programmer fra `node_modules`-katalogen.

For å legge til en ny pakke

```shell
npx elm install pakkenavn
```

Pakker kan finnes på https://package.elm-lang.org/

For den som ikke kjenner til elm fra før: https://guide.elm-lang.org/

## Installer avhengigheter

```shell
poetry shell
```

### Backend

```shell
poetry install
```

### Frontend

Bruker node v14 (samme som i Dockerfile).

```shell
cd frontend
npm ci
```

Da installeres frontend med dependencies fra `package-lock.json`.

(Dersom du får npm error fordi package.json og lock filen ikke er i sync, må du sannsynligvis kjøre `npm install`, for å oppdatere lock filen.)

### Pre-commit

Aktivater [pre-commit](https://pre-commit.com/) hook-er:

```
pre-commit install
```

## Kjør

### Frontend

```shell
cd frontend
npm start
```

### Backend

Lag konfigurasjon i `config.yaml`

#### Redis

[Redis](https://redis.io/) brukes til å cache data slik at vi ikke bombarderer trege API-er.

Kan for eksempel kjøres med docker:

```shell
docker run -e REDIS_PASSWORD=see_config -p 6379:6379 harbor.uio.no/library/registry.redhat.io-rhscl-redis-5-rhel7
```

Fyll cachen med denne kommandoen:

```shell
poetry run mfe_ms/backend/fill_cache.py
```

Eller, for å fylle cachen med data fra fixtures:

```shell
python tests/fill_redis.py
```

#### FastAPI

```shell
poetry run python mfe_ms/backend/api.py
```

Gå til http://localhost:4000

### GraphQL

Hvis `GraphQLConfig.enable_gui` er satt til `True` får man opp et GraphQL GUI på http://localhost:4000/api/graphql/
