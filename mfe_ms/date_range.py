from __future__ import annotations

import datetime
from dataclasses import dataclass
import typing


@dataclass(frozen=True)
class DateRange:
    """Immutable date range"""

    start: datetime.date
    end: datetime.date

    def __post_init__(self) -> None:
        """Post init validation"""
        if not isinstance(self.start, datetime.date):
            raise TypeError(type(self.start))
        if not isinstance(self.end, datetime.date):
            raise TypeError(type(self.end))
        if self.end < self.start:
            raise ValueError(f"{self.end} < {self.start}")

    def intersection(self, other: DateRange) -> typing.Optional[DateRange]:
        try:
            return DateRange(
                start=max(self.start, other.start), end=min(self.end, other.end)
            )
        except ValueError:
            return None

    def intersects(self, other: DateRange) -> bool:
        return self.intersection(other) is not None
