from __future__ import annotations

import itertools
from copy import deepcopy, copy
from typing import (
    Iterable,
    TypeVar,
    Dict,
    Set,
    Callable,
)

from bottint_tree import Tree, build_forest

from dfo_sap_client.models import Orgenhet

T = TypeVar("T")

OrganizationForestT = Dict[int, Tree[Orgenhet]]


def build_organization_forest(
    organizations: Iterable[Orgenhet],
    check_parent: bool = True,
    check_root: bool = True,
    add_branches: bool = True,
) -> OrganizationForestT:
    if add_branches:
        return build_forest(
            organizations,
            id_key="id",
            parent_key="overordn_orgenhet_id",
            check_parent=check_parent,
            check_root=check_root,
        )
    else:
        return {
            x.id: Tree(data=x, id_key="id", parent=None, children=[])
            for x in organizations
        }


def prune(tree: Tree[Orgenhet], *, use_deep_copy: bool = True) -> Tree[Orgenhet]:
    """Remove parent"""
    do_copy: Callable[[T], T] = deepcopy if use_deep_copy else copy  # type: ignore[assignment]
    clone = do_copy(tree)
    clone.parent = None
    clone.data = clone.data.copy(update={"overordn_orgenhet_id": None})
    return clone


def is_child(parent: Tree[T], child: Tree[T]) -> bool:
    if child.parent is None or parent is child:
        return False
    if child.parent == parent:
        return True
    return is_child(parent, child.parent)


def clean(
    forest: OrganizationForestT, to_keep: Set[int], *, use_deep_copy: bool = True
) -> OrganizationForestT:
    """Remove trees not in ``to_keep``, but keep their branches"""
    to_keep = set(to_keep)
    allowed_trees = list(
        prune(tree, use_deep_copy=use_deep_copy)
        for k, tree in forest.items()
        if k in to_keep
        and not any(is_child(parent=forest[keeper], child=tree) for keeper in to_keep)
    )
    return {x.id: x for x in itertools.chain(*allowed_trees)}
