import datetime
import json
import logging
from enum import Enum
from typing import overload, Iterator, Optional, Literal, Type, Dict, Any

import redis
from dfo_sap_client.models import (
    Orgenhet,
    Ansatt,
    Stilling,
    BaseModel_T,
    Terminovervakning,
)

from mfe_ms.config import CacheConfig

logger = logging.getLogger(__name__)


class CacheItemNotFound(Exception):
    pass


def _map_data(cls: Type[BaseModel_T], x: Dict[str, Any]) -> Optional[BaseModel_T]:
    if cls == Orgenhet:
        # Don't try to fix invalid Orgenhet
        return cls.from_dict(x)
    try:
        return cls.from_dict(x)
    except ValueError as e:
        logger.warning("Validation error: %s", e)
        return cls.to_model_class_with_optional_fields().from_dict(x)


class Cache:
    def __init__(self, config: CacheConfig):
        self.config = config
        self.redis = redis.Redis(
            host=config.host,
            port=config.port,
            password=config.password,
            decode_responses=True,
        )

    class Key(str, Enum):
        EMPLOYEES = "mfe_ms_employees"
        POSITIONS = "mfe_ms_positions"
        ORGANIZATIONS = "mfe_ms_organizations"
        TERMINOVERVAKNING = "mfe_ms_terminvarsel"
        LAST_UPDATE = "mfe_ms_last_update"

    class Channel(str, Enum):
        ERROR = "mfe_ms_cache_error"
        REFRESH_PROGRESS = "mfe_ms_cache_refresh"

    class Progress(str, Enum):
        STARTED = "STARTED"
        DONE = "DONE"

    def _get(
        self, key: Key, type_: Type[BaseModel_T], raise_not_found: bool
    ) -> Optional[Iterator[BaseModel_T]]:
        data = self.redis.get(key)
        if data is None:
            if raise_not_found:
                raise CacheItemNotFound(key)
            else:
                return None

        def map_data(x: Dict[str, Any]) -> BaseModel_T | None:
            try:
                return _map_data(type_, x)
            except ValueError as e:
                logger.error(
                    "Invalid data from SAP, please notify the data owners: dropping %s (id=%s): %s",
                    str(type_),
                    x.get("id"),
                    str(e),
                )
                return None

        return filter(None, map(map_data, json.loads(data)))

    @overload
    def get_organizations(self, raise_not_found: Literal[True]) -> Iterator[Orgenhet]:
        pass

    @overload
    def get_organizations(
        self, raise_not_found: Literal[False]
    ) -> Optional[Iterator[Orgenhet]]:
        pass

    @overload
    def get_organizations(self) -> Iterator[Orgenhet]:
        pass

    def get_organizations(
        self, raise_not_found: bool = True
    ) -> Optional[Iterator[Orgenhet]]:
        return self._get(
            key=self.Key.ORGANIZATIONS,
            type_=Orgenhet,
            raise_not_found=raise_not_found,
        )

    @overload
    def get_employees(self, raise_not_found: Literal[True]) -> Iterator[Ansatt]:
        pass

    @overload
    def get_employees(
        self, raise_not_found: Literal[False]
    ) -> Optional[Iterator[Ansatt]]:
        pass

    @overload
    def get_employees(self) -> Iterator[Ansatt]:
        pass

    def get_employees(self, raise_not_found: bool = True) -> Optional[Iterator[Ansatt]]:
        return self._get(
            key=self.Key.EMPLOYEES,
            type_=Ansatt,
            raise_not_found=raise_not_found,
        )

    @overload
    def get_positions(self, raise_not_found: Literal[True]) -> Iterator[Stilling]:
        pass

    @overload
    def get_positions(
        self, raise_not_found: Literal[False]
    ) -> Optional[Iterator[Stilling]]:
        pass

    @overload
    def get_positions(self) -> Iterator[Stilling]:
        pass

    def get_positions(
        self, raise_not_found: bool = True
    ) -> Optional[Iterator[Stilling]]:
        return self._get(
            key=self.Key.POSITIONS,
            type_=Stilling,
            raise_not_found=raise_not_found,
        )

    @overload
    def get_terminovervakning(
        self, raise_not_found: Literal[True]
    ) -> Iterator[Terminovervakning]:
        pass

    @overload
    def get_terminovervakning(
        self, raise_not_found: Literal[False]
    ) -> Optional[Iterator[Terminovervakning]]:
        pass

    @overload
    def get_terminovervakning(self) -> Iterator[Terminovervakning]:
        pass

    def get_terminovervakning(
        self, raise_not_found: bool = True
    ) -> Optional[Iterator[Terminovervakning]]:
        return self._get(
            key=self.Key.TERMINOVERVAKNING,
            type_=Terminovervakning,
            raise_not_found=raise_not_found,
        )

    @property
    def last_update(self) -> Optional[datetime.datetime]:
        if ts := self.redis.get(self.Key.LAST_UPDATE):
            return datetime.datetime.utcfromtimestamp(int(ts))
        return None
