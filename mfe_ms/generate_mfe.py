"""
Main script for producing census data for eValg
"""
import datetime
import itertools
import logging
from collections import defaultdict
from enum import Enum
from http import HTTPStatus
from typing import Dict, Optional, Iterator, Iterable, Union, List, Set, Callable

from dfo_sap_client.models import (
    Ansatt,
    Stilling,
    Innehaver,
    Orgenhet,
    Tilleggsstilling,
    Terminovervakning,
)
from pydantic import BaseModel, Field

from mfe_ms.cache import Cache
from mfe_ms.config import MFEConfig
from mfe_ms.context import MFEContext
from mfe_ms.date_range import DateRange
from mfe_ms.utils import mg_id_to_text, mug_id_to_text

from mfe_ms.tree import build_organization_forest, OrganizationForestT, prune, clean

# Set up logging
logger = logging.getLogger(__name__)


class Termintype(str, Enum):
    tilleggsstilling = "66"  # Utløp tidsbegr.509
    hovedstilling = "72"  # "Utløp tidsbegr.anset"


class Entry(BaseModel):
    ansattnr: str
    navn: str
    startdato: Optional[datetime.date]
    sluttdato: datetime.date
    organisasjonsid: int
    kostnadssted: Optional[str]
    orgkortnavn: Optional[str]
    medarbeidegruppe: str
    medarbeiderundergruppe: str
    stilling_id: int = Field(alias="stillingID")
    stillingskode: Optional[int]
    stillingstittel: Optional[str]
    dellonnsprosent: Optional[float]
    feide_id: Optional[str] = Field(alias="feideID")
    fnr: str
    stillingskategori: Optional[str]
    type_tilleggsstilling: Optional[str] = Field(alias="typeTilleggsstilling")
    avtaledato: Optional[datetime.date]
    kommentar: Optional[str]
    personlig_tittel: Optional[str]
    # Primarily used by UiT
    fornavn: Optional[str]
    etternavn: Optional[str]
    hovedarbeidsforhold: Optional[str]
    adresse: Optional[str]
    poststed: Optional[str]
    postnr: Optional[str]
    campusLokasjon: Optional[str]
    kjonn: Optional[str]
    fodselsdato: Optional[str]

    class Config:
        allow_population_by_field_name = True


class MfeException(Exception):
    http_status_code = HTTPStatus.INTERNAL_SERVER_ERROR


class NotFound(MfeException):
    http_status_code = HTTPStatus.NOT_FOUND


class NoAccess(MfeException):
    http_status_code = HTTPStatus.FORBIDDEN


class InvalidArgument(MfeException):
    http_status_code = HTTPStatus.BAD_REQUEST


def mg_mug_check(config: MFEConfig, employee: Ansatt) -> bool:
    """Check if employee should be skipped."""
    return (
        employee.medarbeidergruppe in config.illegal_mg
        or employee.medarbeiderundergruppe in config.illegal_mug
    )


def should_skip_innehaver(inh: Innehaver) -> bool:
    """
    Check if we should skip this employee based on the info in the Innehaver object

    TODO: Add algorithm when functional design is updated
    """
    return False


def _extract_latest_terminovervakning(
    termintype: Termintype, terminovervakning: Iterable[Terminovervakning]
) -> Optional[Terminovervakning]:
    """Extract item with specified termintype and highest avtaledato."""
    xs = tuple(filter(lambda x: x.termintype == termintype, terminovervakning))
    if not xs:
        return None
    return max(xs, key=lambda x: x.avtaledato or datetime.date.min)


class EmployeeMapper:
    def __init__(
        self,
        *,
        config: MFEConfig,
        positions: Dict[str, Stilling],
        terminovervakning: Optional[Dict[str, Iterable[Terminovervakning]]] = None,
        forest: OrganizationForestT,
        date_range: DateRange,
    ) -> None:
        self.positions = positions
        self.config = config
        self.forest = forest
        self.terminovervakning = terminovervakning or {}
        self.date_range = date_range

    def should_skip_employee(self, employee: Ansatt) -> bool:
        """Return True if the employee should be skipped"""
        if not self.valid_dellonnsprosent(employee):
            return True
        return mg_mug_check(self.config, employee)

    def is_position_in_range(self, position: Union[Ansatt, Tilleggsstilling]) -> bool:
        if position.startdato is None:
            return False
        try:
            position_range = DateRange(
                start=position.startdato,
                end=position.sluttdato,
            )
            return position_range.intersects(self.date_range)
        except ValueError as e:
            logger.info("Invalid date range: %s", e)
            return False

    def should_skip_position(self, position: Union[Ansatt, Tilleggsstilling]) -> bool:
        return not self.is_position_in_range(position=position)

    def valid_dellonnsprosent(self, employee: Ansatt) -> bool:
        """Only accept stilling where dellonnsprosent is more than 50, can be adjustable"""
        dellonnsprosent: int | float = (
            0 if employee.dellonnsprosent is None else employee.dellonnsprosent
        )
        if dellonnsprosent >= self.config.min_dellonsprosent:
            return True

        return (
            dellonnsprosent + self._add_dellonnsprosent(employee.tilleggsstilling)
            >= self.config.min_dellonsprosent
        )

    def _add_dellonnsprosent(
        self, tilleggsstillinger: List[Tilleggsstilling]
    ) -> float | int:
        def should_keep(x: Tilleggsstilling) -> bool:
            p = self.positions.get(str(x.stilling_id))
            if not p:
                return False
            return x.dellonnsprosent is not None and p.organisasjon_id in self.forest

        return sum(x.dellonnsprosent for x in tilleggsstillinger if should_keep(x))

    def map_employee(self, employee: Ansatt) -> Iterable[Entry]:
        if self.should_skip_employee(employee):
            logger.debug(
                "`should_skip_employee` says yes, skipping employee %s", employee.id
            )
            return ()

        main_position = self.positions.get(str(employee.stilling_id))
        if not main_position:
            logger.error(
                "Position with id `%s` not found for employee `%s`",
                employee.stilling_id,
                employee.id,
            )
            return ()

        def do_map(position: Union[None, Stilling, Tilleggsstilling]) -> Entry | None:
            if position is None:
                logger.warning("Position is None for employee `%s`", employee.id)
                return None
            try:
                return self.map_position(
                    employee=employee,
                    position=position,
                )
            except ValueError as e:
                if isinstance(position, Tilleggsstilling):
                    pos_id = position.stilling_id
                else:
                    pos_id = position.id
                logger.error(
                    "Error while creating entry, dropping position `%s` for employee `%s`: %s",
                    pos_id,
                    employee.id,
                    e,
                )
            return None

        positions: Iterator[None | Stilling | Tilleggsstilling] = itertools.chain(
            (main_position,), employee.tilleggsstilling
        )
        mapped = (do_map(x) for x in positions)
        return (x for x in mapped if x is not None)

    def map_position(
        self, employee: Ansatt, position: Union[Stilling, Tilleggsstilling]
    ) -> Optional[Entry]:
        data_source: Ansatt | Tilleggsstilling
        if isinstance(position, Tilleggsstilling):
            pos_id = str(position.stilling_id)
            data_source = position
            ekstra_stilling = position.ekstra_stilling
        else:
            pos_id = str(position.id)
            data_source = employee
            ekstra_stilling = None

        if self.should_skip_position(data_source):
            logger.debug(
                "`should_skip_position` says yes, skipping position `%s` for employee `%s`",
                pos_id,
                employee.id,
            )
            return None

        if not pos_id or not (pos := self.positions.get(pos_id)):
            logger.error(
                "Position with id `%s` not found for employee `%s`", pos_id, employee.id
            )
            return None

        if any(should_skip_innehaver(x) for x in pos.innehaver):
            logger.debug(
                "`should_skip_innehaver` says yes, skipping employee `%s`",
                employee.id,
            )
            return None

        if not pos.organisasjon_id or not (org := self.forest.get(pos.organisasjon_id)):
            logger.debug(
                "Organization `%s` not in forest of allowed organizations, skipping",
                pos.organisasjon_id,
            )
            return None

        dellonnsprosent: int | float | None
        if isinstance(position, Tilleggsstilling):
            dellonnsprosent = position.dellonnsprosent
        else:
            dellonnsprosent = (
                employee.dellonnsprosent
                if employee.organisasjon_id in self.forest
                else None
            )
            dellonnsprosent_sum = self._add_dellonnsprosent(employee.tilleggsstilling)
            if self.config.add_all_dellonnsprosent:
                dellonnsprosent = 0 if dellonnsprosent is None else dellonnsprosent
                dellonnsprosent += dellonnsprosent_sum
        navn: str | None
        if xs := list(filter(None, [employee.fornavn, employee.etternavn])):
            navn = " ".join(xs)
        else:
            navn = None

        if self.config.add_terminovervakning:
            terminovervakning = self.terminovervakning.get(employee.id, ())
            if isinstance(position, Tilleggsstilling):
                termintype = Termintype.tilleggsstilling
            else:
                termintype = Termintype.hovedstilling

            if x := _extract_latest_terminovervakning(termintype, terminovervakning):
                avtaledato = x.avtaledato
                kommentar = (
                    "\n\n".join(filter(None, (x.tekst1, x.tekst2, x.tekst3))) or None
                )
            else:
                logger.warning(
                    "avtaledato not found for employee `%s`, position `%s`",
                    employee.id,
                    pos_id,
                )
                avtaledato = None
                kommentar = None
        else:
            avtaledato = None
            kommentar = None

        if self.config.include_uit_fields:
            uit_fields = {
                "fornavn": employee.fornavn,
                "etternavn": employee.etternavn,
                "hovedarbeidsforhold": "H" if isinstance(pos, Stilling) else "T",
                "adresse": employee.privat_postadresse,
                "poststed": employee.privat_poststed,
                "postnr": employee.privat_postnr,
                "campusLokasjon": employee.pdo,
                "kjonn": employee.kjonn,
                "fodselsdato": employee.fdato,
            }
        else:
            uit_fields = {}

        return Entry(
            ansattnr=employee.id,
            navn=navn,
            startdato=data_source.startdato,
            sluttdato=data_source.sluttdato,
            organisasjonsid=pos.organisasjon_id,
            kostnadssted=org.data.org_kostnadssted,
            orgkortnavn=org.data.org_kortnavn,
            medarbeidegruppe=mg_id_to_text(employee.medarbeidergruppe),
            medarbeiderundergruppe=mug_id_to_text(employee.medarbeiderundergruppe),
            stilling_id=pos.id,
            stillingskode=pos.stillingskode,
            stillingstittel=pos.stillingstittel,
            dellonnsprosent=dellonnsprosent,
            feide_id=employee.ekstern_ident,
            fnr=employee.fnr,
            stillingskategori=(
                pos.stillingskat[0].stillingskat_betegn if pos.stillingskat else None
            ),
            type_tilleggsstilling=ekstra_stilling,
            avtaledato=avtaledato,
            kommentar=kommentar,
            personlig_tittel=employee.tittel,
            **uit_fields,
        )


def process_data(
    *,
    config: MFEConfig,
    positions: Dict[str, Stilling],
    employees: Iterator[Ansatt],
    orgunits: Dict[str, Orgenhet],
    terminovervakning: Optional[Dict[str, Iterable[Terminovervakning]]] = None,
    roots: Optional[Iterable[int]] = None,
    include_branches: bool = True,
    get_allowed_ids: Optional[Callable[[OrganizationForestT], Set[int]]] = None,
    date_range: DateRange,
) -> Iterator[Entry]:
    """
    Process data and write to file.

    Loop over positions, then loop over employees with that position.
    """
    if roots:
        roots = set(roots)
    elif not include_branches:
        raise InvalidArgument
    forest = build_organization_forest(orgunits.values(), add_branches=include_branches)
    if roots:
        try:
            orgs = {}
            for root in roots:
                if root not in orgs:
                    for pruned in prune(forest[root]).values():
                        orgs[pruned.id] = pruned
            forest = build_organization_forest(orgs.values())
        except KeyError as e:
            raise NotFound(*e.args) from e
    if get_allowed_ids is not None:
        allowed = get_allowed_ids(forest)
        if not allowed:
            raise NoAccess
        forest = clean(forest, allowed, use_deep_copy=False)
    if roots is not None and any(root not in forest for root in roots):
        raise NoAccess

    mapper = EmployeeMapper(
        config=config,
        positions=positions,
        forest=forest,
        terminovervakning=terminovervakning,
        date_range=date_range,
    )
    return (x for xs in map(mapper.map_employee, employees) for x in xs)


def _make_terminovervakning_dict(
    cache: Cache,
) -> Dict[str, Iterable[Terminovervakning]]:
    terminovervakning = defaultdict(list)
    for x in cache.get_terminovervakning():
        terminovervakning[x.id].append(x)
    return dict(terminovervakning)


def generate_data(
    *,
    context: MFEContext,
    date_range: DateRange,
    roots: Optional[List[int]] = None,
    include_branches: bool = True,
    get_allowed_ids: Optional[Callable[[OrganizationForestT], Set[int]]] = None,
) -> Iterator[Entry]:
    """Gets data and processes it"""
    logger.info("Genrating data")
    if not isinstance(context, MFEContext):
        raise TypeError
    cache = context.cache
    # Get data
    cached_positions = cache.get_positions()
    cached_orgunits = cache.get_organizations()
    cached_employees = cache.get_employees()
    if context.config.add_terminovervakning:
        terminovervakning = _make_terminovervakning_dict(cache)
    else:
        terminovervakning = None

    positions = {str(i.id): i for i in cached_positions}
    orgunits = {str(i.id): i for i in cached_orgunits}

    # process data
    return process_data(
        config=context.config,
        positions=positions,
        employees=cached_employees,
        orgunits=orgunits,
        roots=roots,
        include_branches=include_branches,
        terminovervakning=terminovervakning,
        get_allowed_ids=get_allowed_ids,
        date_range=date_range,
    )
