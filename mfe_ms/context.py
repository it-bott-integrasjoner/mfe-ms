import functools
from typing import Optional

from authlib.integrations.starlette_client import OAuth
from dfo_sap_client import SapClient

from mfe_ms.config import MFEMsConfigLoader, get_config_loader
from mfe_ms.cache import Cache


class MFEContext:
    def __init__(
        self,
        sap: SapClient,
        cache: Cache,
        oauth: OAuth,
        config_loader: MFEMsConfigLoader,
    ):
        self.sap = sap
        self.cache = cache
        self.oauth = oauth
        self.config_loader = config_loader
        self.config = config_loader.mfe_ms


def make_context(config_loader: MFEMsConfigLoader) -> MFEContext:
    sap = SapClient(**config_loader.mfe_ms.sap.dict(exclude_unset=True))
    sap.ignore_required_model_fields = True
    cache = Cache(config_loader.mfe_ms.cache)
    oauth = OAuth()
    return MFEContext(sap=sap, cache=cache, oauth=oauth, config_loader=config_loader)


@functools.lru_cache()
def get_context(filename: Optional[str] = None) -> MFEContext:
    config_loader = get_config_loader(filename)
    context = make_context(config_loader)
    return context
