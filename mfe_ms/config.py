import functools
import os
import sys
import typing
import uuid
from enum import Enum
from typing import Optional, Set, Union

import jose.constants
import yaml
from pydantic import (
    BaseModel,
    HttpUrl,
    AnyHttpUrl,
    parse_obj_as,
    root_validator,
)

SERVICE_NAME = __name__.split(".")[0]

FILES_ETC = os.path.join("" if sys.prefix == "/usr" else sys.prefix, "etc")

CONFIG_NAME = "config.yaml"
CONFIG_LOOKUP_ORDER = [
    os.getcwd(),
    os.path.expanduser(f"~/{SERVICE_NAME}"),
    os.path.join(FILES_ETC, f"{SERVICE_NAME}"),
]
CONFIG_ENVIRON = f"{SERVICE_NAME.upper()}_CONFIG"


class DfoSapEndpoints(BaseModel):
    baseurl: HttpUrl
    ansatt_url: typing.Optional[str]
    orgenhet_url: typing.Optional[str]
    stilling_url: typing.Optional[str]
    ansatte_terminovervakning: typing.Optional[str]


class DfoSapConfig(BaseModel):
    urls: DfoSapEndpoints
    tokens: typing.Optional[typing.Dict[str, typing.Dict[str, str]]]
    headers: typing.Optional[typing.Dict[str, str]]
    match_server_header: typing.Optional[bool] = False
    ansatte_split_id: typing.Optional[int | typing.Iterable[int]] = None
    stillinger_split_id: typing.Optional[int | typing.Iterable[int]] = None


class DataportenConfig(BaseModel):
    well_known_url: HttpUrl = parse_obj_as(
        HttpUrl, "https://auth.dataporten.no/.well-known/openid-configuration"
    )
    client_id: str
    client_secret: str
    extended_userinfo_endpoint: HttpUrl = parse_obj_as(
        HttpUrl, "https://api.dataporten.no/userinfo/v1/userinfo"
    )
    groups_endpoint: HttpUrl = parse_obj_as(
        HttpUrl, "https://groups-api.dataporten.no/groups/me/groups"
    )


class GraphQLConfig(BaseModel):
    enable_gui: bool = False
    debug: bool = False


class AuthorizationType(str, Enum):
    entitlements = "entitlements"
    lists = "lists"


class ListsAuthorization(BaseModel):
    allowed_users: Optional[Set[str]]
    allowed_groups: Optional[Set[str]]

    @root_validator
    def check_allowed(
        cls, values: typing.Dict[str, typing.Any]
    ) -> typing.Dict[str, typing.Any]:
        users_key = "allowed_users"
        groups_key = "allowed_groups"
        users = values.get(users_key)
        groups = values.get(groups_key)
        if not users and not groups:
            raise ValueError(
                f"{users_key} and {groups_key} can't both be empty or None"
            )

        # Make sure we don't accidentally open up for all users or all groups
        if users is None and groups:
            values[users_key] = []
        if groups is None and users:
            values[groups_key] = []

        return values


class EntitlementsAuthorization(BaseModel):
    allowed_super_user_access: Optional[Set[str]]
    allowed_org_user_entitlement_prefix: Optional[str]


class AuthConfig(BaseModel):
    secret_key: str
    algorithm: str = jose.constants.ALGORITHMS.HS256
    dataporten: DataportenConfig
    authorization_type: AuthorizationType
    authorization_config: Union[ListsAuthorization, EntitlementsAuthorization]
    api_key: Optional[uuid.UUID]


class CacheConfig(BaseModel):
    host: str = "localhost"
    port: int = 6379
    password: Optional[str]
    """Minimum amount of seconds between cache refreshes"""
    minimum_time_between_refreshes: int = 3600


class Instance(str, Enum):
    uio = "uio"
    uib = "uib"
    uit = "uit"
    ntnu = "ntnu"


class MFEConfig(BaseModel):
    """mfe-ms configuration"""

    auth: AuthConfig
    illegal_mug: typing.Tuple[str, ...] = ()
    illegal_mg: typing.Tuple[str, ...] = ()
    sap: DfoSapConfig
    graphql: GraphQLConfig = GraphQLConfig()
    base_url: typing.Optional[AnyHttpUrl] = None
    add_all_dellonnsprosent: bool
    min_dellonsprosent: float = 50
    cache: CacheConfig = CacheConfig()
    instance_name: Instance
    add_terminovervakning: bool = False
    include_uit_fields: bool = False


def iter_config_locations(
    filename: str = CONFIG_NAME, lookup_order: typing.Optional[typing.List[str]] = None
) -> typing.Iterator[str]:
    if CONFIG_ENVIRON in os.environ:
        yield os.path.abspath(os.environ[CONFIG_ENVIRON])
    for path in lookup_order or CONFIG_LOOKUP_ORDER:
        yield os.path.abspath(os.path.join(path, filename))


class MFEMsConfigLoader(BaseModel):
    """mfe-ms configuration loader"""

    mfe_ms: MFEConfig
    logging: Optional[typing.Dict[str, typing.Any]]
    sentry: Optional[
        typing.Dict[str, typing.Any]
    ]  # passed directly to sentry_sdk.init()

    @classmethod
    def from_yaml(cls, yamlstr: str) -> "MFEMsConfigLoader":
        return cls(**yaml.load(yamlstr, Loader=yaml.FullLoader))

    @classmethod
    def from_file(cls, filename: str) -> "MFEMsConfigLoader":
        with open(filename, "r") as f:
            return cls.from_yaml(f.read())


@functools.lru_cache()
def get_config_loader(filename: Optional[str] = None) -> MFEMsConfigLoader:
    if filename is None:
        for filename in filter(os.path.exists, iter_config_locations()):
            break
    if filename:
        return MFEMsConfigLoader.from_file(filename)
    raise ValueError("Configuration file not found")


@functools.lru_cache()
def get_config(filename: str | None = None) -> MFEConfig:
    return get_config_loader(filename).mfe_ms
