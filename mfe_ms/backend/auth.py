import secrets

from enum import Enum
from http import HTTPStatus
from typing import Optional, Union
from uuid import UUID

from fastapi import Request, HTTPException
from fastapi.security import (
    HTTPBearer,
    HTTPAuthorizationCredentials,
)
from starlette.authentication import BaseUser

from mfe_ms.config import AuthConfig


class Scope(str, Enum):
    authorized = "authorized"
    superuser = "superuser"
    api_key = "api_key"


async def _authorized_user(
    *, request: Request, auto_error: bool = True
) -> Optional[BaseUser]:
    if request.user and Scope.authorized in request.auth.scopes:
        assert isinstance(request.user, BaseUser)
        return request.user
    if auto_error:
        raise HTTPException(HTTPStatus.UNAUTHORIZED)
    return None


async def _authorize_api_key(
    *, request: Request, api_key: UUID, scheme_name: str
) -> HTTPAuthorizationCredentials | None:
    auth = HTTPBearer(auto_error=False)
    if credentials := await auth(request=request):
        # Use secrets.compare_digest to prevent timing attacks
        if not secrets.compare_digest(credentials.credentials, str(api_key)):
            raise HTTPException(HTTPStatus.FORBIDDEN)
        request.auth.scopes = [Scope.api_key, Scope.authorized]
        credentials.scheme = scheme_name
        return credentials
    return None


class ApiKeyOrAuthorizedUser:
    def __init__(self, *, config: AuthConfig, auto_error: bool = True):
        self.scheme_name = "HTTPBearer"
        self.config = config
        self.auto_error = auto_error

    async def __call__(
        self, request: Request
    ) -> Union[None, HTTPAuthorizationCredentials, BaseUser]:
        if api_key := self.config.api_key:
            if credentials := await _authorize_api_key(
                request=request,
                api_key=api_key,
                scheme_name=self.scheme_name,
            ):
                return credentials

        if user := await _authorized_user(auto_error=False, request=request):
            return user

        if self.auto_error:
            raise HTTPException(HTTPStatus.UNAUTHORIZED)
        return None
