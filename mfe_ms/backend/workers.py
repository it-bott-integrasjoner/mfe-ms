from uvicorn.workers import UvicornWorker


class BehindProxyWorker(UvicornWorker):
    CONFIG_KWARGS = {"proxy_headers": True}
