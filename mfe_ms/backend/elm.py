from __future__ import annotations

import itertools
import os
import re

from typing import Iterable, Dict, Optional, TypeVar, Callable, Tuple, Iterator
from os import linesep

import fastapi.openapi.models as openapi

from mfe_ms.context import get_context

T = TypeVar("T")
ELM_TYPE_RE = re.compile(r"[A-Z][A-Za-z-0-9]*$")
ELM_VARIABLE_RE = re.compile(r"[a-z][A-Za-z-0-9]*$")


def validate_type_name(name: str | None) -> str:
    assert isinstance(name, str)
    parts = name.split(".")
    for part in parts:
        if not ELM_TYPE_RE.match(part):
            raise ValueError(name)
    return name


def validate_variable_name(name: str) -> str:
    if not ELM_VARIABLE_RE.match(name):
        raise ValueError(name)
    return name


def to_pascal(s: str) -> str:
    xs = s.split("_")
    return "".join(x[0].upper() + x[1:] for x in xs)


def to_camel(s: str) -> str:
    pascal = to_pascal(s)
    return pascal[0].lower() + pascal[1:]


def partition(
    pred: Callable[[T], bool], iterable: Iterable[T]
) -> Tuple[Iterator[T], Iterator[T]]:
    evaluations = ((pred(x), x) for x in iterable)
    falses, trues = itertools.tee(evaluations)
    return (
        (x for (cond, x) in falses if not cond),
        (x for (cond, x) in trues if cond),
    )


class ElmType:
    def __init__(
        self,
        *,
        is_list: bool,
        name: str,
        required: bool,
        elm_type: Optional[str] = None,
    ) -> None:
        self.is_list = is_list
        self.elm_type = validate_type_name(elm_type or to_pascal(name))
        self.name = validate_variable_name(to_camel(name))
        self.required = required

    def __str__(self) -> str:
        type_ = self.elm_type
        if self.is_list:
            type_ = f"List {type_}"
        if self.required:
            return type_
        return f"Maybe ({type_})"

    @classmethod
    def from_parameter(cls, param: openapi.Parameter) -> ElmType:
        assert isinstance(param.schema_, openapi.Schema)
        assert isinstance(param.schema_.type, str)
        elm_types: Dict[str, str] = {
            "string": "String",
            "boolean": "Bool",
            "integer": "Int",
            "float": "Float",
        }
        is_list = param.schema_.type == "array"
        title = None
        if is_list:
            assert isinstance(param.schema_.items, openapi.Schema)
            assert isinstance(param.schema_.items.type, str)
            type_ = elm_types[param.schema_.items.type]
        elif param.schema_.type == "string" and param.schema_.format:
            if param.schema_.format == "date":
                type_ = "date"
                title = "Api.Date.Date"
            else:
                raise NotImplementedError(f"String format `{param.schema_.format}`")
        else:
            type_ = elm_types[param.schema_.type]
        return ElmType(
            name=type_,
            required=is_list or bool(param.required) and param.schema_.default is None,
            is_list=is_list,
            elm_type=title,
        )


class QueryParameter:
    def __init__(self, *, name: str, title: str, type_: ElmType) -> None:
        assert name
        assert title
        self.type_ = type_
        self.name = name
        self.title = to_pascal(title)

    def __str__(self) -> str:
        return self.title

    @property
    def parameter_function(self) -> str:
        arg_type = self.type_.elm_type
        postfix = ""
        if self.type_.required:
            postfix = " << Just"
        else:
            arg_type = f"Maybe ({arg_type})"
        return linesep.join(
            (
                f"{self.name} : {arg_type} -> Parameter {self.type_.elm_type}",
                f"{self.name} = Api.Form.{self.type_.name} name {self.title}{postfix}",
            )
        )


class ApiModule:
    def __init__(
        self,
        *,
        operation: openapi.Operation,
        path: str,
        constructors: Iterable[QueryParameter],
    ) -> None:
        validate_type_name(operation.operationId)
        self.operation = operation
        self.path = path
        self.constructors = list(constructors)

    @classmethod
    def from_openapi(cls, path: str, operation: openapi.Operation) -> ApiModule:
        def qp(x: openapi.Parameter | openapi.Reference) -> QueryParameter:
            assert isinstance(x, openapi.Parameter)
            assert isinstance(x.schema_, openapi.Schema)
            assert isinstance(x.schema_.title, str)
            return QueryParameter(
                name=x.name, title=x.schema_.title, type_=ElmType.from_parameter(x)
            )

        constructors = [qp(x) for x in operation.parameters or ()]
        return ApiModule(
            constructors=constructors,
            operation=operation,
            path=path,
        )

    def __str__(self) -> str:
        return (linesep * 3).join(
            filter(
                None,
                (
                    self.module,
                    self.parameter_name_type,
                    self.parameter_name_function,
                    self.type_alias,
                    self.parameter_functions,
                    self.query_parameters_function,
                    self.api_path,
                    self.uri,
                ),
            )
        ) + linesep

    @property
    def file_path(self) -> str:
        return f"{self.module_name.replace('.', os.sep)}.elm"

    @property
    def parameter_name_type(self) -> str | None:
        if not self.constructors:
            return None
        return f"type ParameterName{linesep}    = {(linesep + '    | ').join(map(str, self.constructors))}"

    @property
    def parameter_name_function(self) -> str | None:
        if not self.constructors:
            return None
        cases = f"{linesep}".join(
            f'        {x.title} -> "{x.name}"' for x in self.constructors
        )
        return linesep.join(
            ["name : ParameterName -> String", "name n =", "    case n of", cases]
        )

    @property
    def type_alias(self) -> str | None:
        if not self.constructors:
            return None
        record = (
            "{ " + ", ".join(f"{x.name} : {x.type_}" for x in self.constructors) + " }"
        )
        return (linesep * 3).join(
            (
                f"type alias Parameters = {record}",
                "type alias Parameter value = Api.Form.Parameter ParameterName value",
            )
        )

    @property
    def parameter_functions(self) -> str | None:
        if not self.constructors:
            return None
        return (linesep * 3).join(x.parameter_function for x in self.constructors)

    @property
    def query_parameters_function(self) -> str | None:
        if not self.constructors:
            return None

        def elm_param(constructor: QueryParameter) -> str:
            param_name = f"parameters.{constructor.name}"
            if constructor.type_.is_list:
                return f"List.map (Api.Form.toQueryParameter << {constructor.name}) {param_name}"
            return f"Api.Form.toQueryParameter ({constructor.name} {param_name})"

        elm_param_sep = f"{linesep}{' ' * 4}, "
        params, list_params = partition(lambda x: x.type_.is_list, self.constructors)
        elm_params = (
            f"""[ {elm_param_sep.join(elm_param(x) for x in params)}{linesep}    ]"""
        )
        elm_list_params = f"{linesep}{' ' * 8}".join(map(elm_param, list_params))
        if not elm_list_params:
            elm_list_params = "[]"
        return linesep.join(
            (
                "queryParameters : Parameters -> List Url.Builder.QueryParameter",
                "queryParameters parameters =",
                f"    {elm_params}",
                f"        ++ {elm_list_params}",
                "        |> List.filterMap identity",
            )
        )

    @property
    def module_name(self) -> str:
        return f"Api.{self.operation.operationId}"

    @property
    def module(self) -> str:
        return linesep.join(
            filter(
                None,
                (
                    f"module {self.module_name} exposing",
                    self.exports,
                    self.imports,
                ),
            )
        )

    @property
    def api_path(self) -> str:
        return linesep.join(
            (
                "path : String",
                f'path = "{self.path}"',
            )
        )

    @property
    def uri(self) -> str | None:
        if not self.constructors:
            return None
        return linesep.join(
            (
                "uri : Parameters -> String",
                "uri parameters =",
                "    Url.Builder.relative [ path ] (queryParameters parameters)",
            )
        )

    @property
    def imports(self) -> str | None:
        if not self.constructors:
            return None
        return linesep.join(
            (
                "import Api.Date",
                "import Api.Form",
                "import Url.Builder",
            )
        )

    @property
    def exports(self) -> str:
        if not self.constructors:
            return "    (uri)"
        return (
            "    ( "
            + f"{linesep}    , ".join(
                sorted(
                    [
                        "Parameter",
                        "ParameterName",
                        "Parameters",
                        "path",
                        "queryParameters",
                        "uri",
                    ]
                    + [x.name for x in self.constructors]
                )
            )
            + f"{linesep}    )"
        )


def generate_elm(api_def: openapi.OpenAPI, path: str) -> ApiModule | None:
    """Only handles GET endpoints"""
    endpoint = api_def.paths[path].get
    if not endpoint:
        return None
    return ApiModule.from_openapi(operation=endpoint, path=path)


def main() -> None:
    import argparse, sys, pathlib  # noqa

    parser = argparse.ArgumentParser(description="Generate elm modules for api")
    parser.add_argument(
        "--output-directory",
        type=str,
        nargs="?",
        help="the directory to write elm files to",
    )
    args = parser.parse_args()
    import mfe_ms.backend.api

    _, api = mfe_ms.backend.api.init(get_context())

    api_def = openapi.OpenAPI(**api.openapi())
    for path in api_def.paths:
        elm_module = generate_elm(api_def, path)
        if not elm_module:
            continue
        if args.output_directory:
            output_directory = pathlib.Path(args.output_directory)
            file = output_directory / pathlib.Path(elm_module.file_path)
            file.parent.mkdir(parents=True, exist_ok=True)
            with file.open("w", encoding="utf-8") as f:
                f.write(str(elm_module))
        else:
            sys.stdout.write(str(elm_module))


if __name__ == "__main__":
    main()
