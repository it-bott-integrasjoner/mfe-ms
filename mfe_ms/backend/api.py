import json
import logging
import time
import uuid
from http import HTTPStatus
from typing import List, Optional, Set, TYPE_CHECKING, Tuple, Callable, Awaitable

import pydantic
import fastapi.params
from fastapi import FastAPI, Request, Response, HTTPException, Depends, Query
from fastapi.encoders import jsonable_encoder
from fastapi.staticfiles import StaticFiles
from gunicorn.util import http_date
from starlette.applications import Starlette
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.middleware.sessions import SessionMiddleware
from starlette.responses import RedirectResponse, JSONResponse
from starlette.routing import Router
from datetime import date
import mfe_ms.backend.routes.dataporten as dataporten
import mfe_ms.backend.routes.graphql as graphql
from mfe_ms.backend.auth import Scope, ApiKeyOrAuthorizedUser
from mfe_ms.backend.utils import get_allowed_organizations_for_scopes
from mfe_ms.cache import CacheItemNotFound

from mfe_ms.context import get_context, MFEContext
from mfe_ms.date_range import DateRange
from mfe_ms.generate_mfe import generate_data, Entry, MfeException
from mfe_ms.tree import OrganizationForestT


if TYPE_CHECKING:
    ErrorStatus = int
else:
    ErrorStatus = pydantic.conint(ge=400, lt=600)


class Error(pydantic.BaseModel):
    status: ErrorStatus
    detail: Optional[str]


logger = logging.getLogger(__name__)


def init(context: MFEContext) -> Tuple[FastAPI, FastAPI]:
    logger.info("Loading mfe-ms backend app")

    if context.config.auth.api_key == uuid.UUID("00000000-0000-0000-0000-000000000000"):
        logger.error("API key is all zeroes, check config")

    app = FastAPI(docs_url=None, redoc_url=None)

    # Need same_site=lax to allow session cookie to be set on redirect to dataporten
    app.add_middleware(
        SessionMiddleware, secret_key=context.config.auth.secret_key, same_site="lax"
    )

    api = FastAPI()

    app.mount("/api", api, name="api")

    @app.get("/api", include_in_schema=False)
    @api.get("/", include_in_schema=False)
    def api_home() -> RedirectResponse:
        return RedirectResponse(url=app.url_path_for("api", path=api.docs_url))

    frontend = Starlette()
    frontend.mount(
        "/",
        StaticFiles(directory="frontend/dist", html=True),
        name="frontend",
    )

    app.mount("/home/", frontend)

    graphql_routes, _ = graphql.routes(context=context)

    api.mount("/graphql/", graphql_routes, name="graphql")

    @app.get("/")
    @app.get("/home", name="home")
    def home() -> RedirectResponse:
        return RedirectResponse(
            url=app.url_path_for("frontend", path="/"), status_code=HTTPStatus.FOUND
        )

    @app.get("/health", response_class=JSONResponse)
    def health() -> JSONResponse:
        """Health endpoint"""
        return JSONResponse(
            {
                "metadata": {
                    "updated": time.time_ns() // 1_000_000,  # unix time in milliseconds
                    "health-file-version": 3,  # zabbix monitor format version
                },
            }
        )

    dataporten_routes, auth_backend = dataporten.routes(
        router=app.router, context=context
    )
    app.mount("/auth/dataporten/", dataporten_routes, name="dataporten")

    @app.get("/flags.json")
    def flags_js() -> Response:
        flags = dict(
            basePath=app.url_path_for("frontend", path="/"),
            apiPath=app.url_path_for("api", path="/"),
            loginPath=app.url_path_for("dataporten", path="/login"),
            logoutPath=app.url_path_for("dataporten", path="/logout"),
            apiDocsPath=app.url_path_for("api", path=api.docs_url),
            instance=context.config.instance_name,
        )
        return Response(json.dumps(flags, indent=2), media_type="application/json")

    async def require_user(
        request: Request, call_next: Callable[[Request], Awaitable[Response]]
    ) -> Response:
        user = request.user
        if user.is_authenticated is not True:
            return Response(status_code=HTTPStatus.UNAUTHORIZED)

        return await call_next(request)

    graphql_routes.add_middleware(BaseHTTPMiddleware, dispatch=require_user)
    api.add_middleware(AuthenticationMiddleware, backend=auth_backend)

    @api.exception_handler(HTTPException)
    async def http_exception_handler(
        request: Request, exc: HTTPException
    ) -> JSONResponse:
        content = Error(status=exc.status_code, detail=exc.detail)
        return JSONResponse(
            status_code=exc.status_code, content=jsonable_encoder(content)
        )

    @api.exception_handler(MfeException)
    async def not_found_handler(request: Request, exc: MfeException) -> JSONResponse:
        content = Error(
            status=exc.http_status_code, detail=str(exc) or exc.__class__.__name__
        )
        return JSONResponse(
            status_code=content.status, content=jsonable_encoder(content)
        )

    @api.get(
        "/manntall",
        response_model=List[Entry],
        responses={code: {"model": Error} for code in (401, 403, 404)},
        summary="Generate census data for eValg",
        operation_id="GetManntall",  # operation_id is used by the elm code generator
        response_description="A list of voters. The `Last-Modified` header contains the date and time when the source data was last fetched.",
    )
    def get_mfe(
        response: Response,
        request: Request,
        mdate: Optional[date] = Query(None),
        valgsslutt: Optional[date] = Query(None),
        recursive: Optional[bool] = Query(True),
        root: Optional[List[int]] = Query(None),
        credentials: fastapi.params.Depends = Depends(
            ApiKeyOrAuthorizedUser(config=context.config.auth)
        ),
    ) -> JSONResponse:
        if mdate is None:
            mdate = date.today()
        if valgsslutt is None:
            valgsslutt = mdate
        get_allowed_scopes: Callable[[OrganizationForestT], Set[int]] | None
        try:
            scopes = set(request.auth.scopes)
            if not {Scope.superuser, Scope.api_key} & scopes:

                def get_allowed_scopes(forest: OrganizationForestT) -> Set[int]:
                    if allowed_ids := get_allowed_organizations_for_scopes(
                        scopes, forest
                    ):
                        return allowed_ids
                    else:
                        raise HTTPException(
                            status_code=HTTPStatus.FORBIDDEN, detail=root
                        )

            else:
                get_allowed_scopes = None

            if last_update := context.cache.last_update:
                headers = {"last-modified": http_date(last_update.timestamp())}
            else:
                headers = None

            return JSONResponse(
                content=jsonable_encoder(
                    generate_data(
                        context=context,
                        date_range=DateRange(start=mdate, end=valgsslutt),
                        roots=root,
                        get_allowed_ids=get_allowed_scopes,
                        include_branches=bool(recursive),
                    )
                ),
                headers=headers,
            )

        except (CacheItemNotFound, pydantic.ValidationError) as e:
            raise HTTPException(
                status_code=HTTPStatus.INTERNAL_SERVER_ERROR, detail=str(e)
            )

    # We don't want FastAPI to do magic slash redirects
    def disable_redirect_slashes(route: Router) -> None:
        route.redirect_slashes = False
        for r in route.routes:
            if hasattr(r, "app") and isinstance(r.app, Starlette):
                disable_redirect_slashes(r.app.router)

    disable_redirect_slashes(app.router)
    return app, api


def main() -> None:
    # For development only
    import uvicorn

    logging.basicConfig(level=logging.NOTSET)
    logging.getLogger("websockets").setLevel(logging.WARNING)
    app, _ = init(get_context())
    uvicorn.run(
        app, host="0.0.0.0", port=4000, proxy_headers=True, forwarded_allow_ips=["*"]
    )


if __name__ == "__main__":
    main()
