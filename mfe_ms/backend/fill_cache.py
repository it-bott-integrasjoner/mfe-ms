#!/usr/bin/env python3

import json
import logging
import logging.config
import time

from dfo_sap_client import SapClient
from fastapi.encoders import jsonable_encoder

from mfe_ms.cache import Cache
from mfe_ms.config import get_config_loader
from mfe_ms.context import MFEContext, make_context

logger = logging.getLogger(__name__)


def _get_all_employees_as_json(sap: SapClient) -> str:
    ansatte = sap.get_all_ansatte() or []
    return json.dumps(jsonable_encoder(ansatte))


def _get_all_stillinger_as_json(sap: SapClient) -> str:
    stillinger = sap.get_all_stillinger() or []
    return json.dumps(jsonable_encoder(stillinger))


def _get_all_orgenheter_as_json(sap: SapClient) -> str:
    units = sap.get_all_orgenheter()
    return json.dumps(jsonable_encoder(units))


def _get_ansatte_terminovervakning(sap: SapClient) -> str:
    terminovervakning = sap.get_ansatte_terminovervakning() or []
    return json.dumps(jsonable_encoder(jsonable_encoder(terminovervakning)))


def fill_cache(context: MFEContext) -> None:
    logger.info("Filling cache")
    redis = context.cache.redis
    with redis.pipeline() as pipe:
        logger.info("Fetching employees")
        data = _get_all_employees_as_json(context.sap)
        pipe.set(Cache.Key.EMPLOYEES, data)

        logger.info("Fetching positions")
        data = _get_all_stillinger_as_json(context.sap)
        pipe.set(Cache.Key.POSITIONS, data)

        logger.info("Fetching organizations")
        data = _get_all_orgenheter_as_json(context.sap)
        pipe.set(Cache.Key.ORGANIZATIONS, data)

        if context.config.add_terminovervakning:
            logger.info("Fetching terminovervakning")
            data = _get_ansatte_terminovervakning(context.sap)
            pipe.set(Cache.Key.TERMINOVERVAKNING, data)

        pipe.set(Cache.Key.LAST_UPDATE, int(time.time()))
        pipe.execute()


def main() -> None:
    config_loader = get_config_loader()
    if config_loader.logging:
        logging.config.dictConfig(config_loader.logging)
    else:
        logging.basicConfig(level=logging.INFO)

    if config_loader.sentry:
        logger.info("Setting up sentry")
        import sentry_sdk

        sentry_sdk.init(**config_loader.sentry)
    else:
        logger.info("Sentry not configured")

    context = make_context(config_loader=config_loader)

    fill_cache(context)


if __name__ == "__main__":
    main()
