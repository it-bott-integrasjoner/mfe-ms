import logging

from fastapi import FastAPI
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

import mfe_ms.backend.api as api
from mfe_ms.context import get_context, MFEContext

logger = logging.getLogger(__name__)


def create_app(context: MFEContext | None = None) -> FastAPI | SentryAsgiMiddleware:
    if context is None:
        context = get_context()
    app: FastAPI | SentryAsgiMiddleware
    app, _ = api.init(context=context)

    if context.config_loader.sentry:
        logger.info("Setting up sentry")
        import sentry_sdk

        sentry_sdk.init(**context.config_loader.sentry)
        app = SentryAsgiMiddleware(app=app)
    return app
