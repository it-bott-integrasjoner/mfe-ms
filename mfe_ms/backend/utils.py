from typing import Set, Collection

from .auth import Scope
from ..tree import OrganizationForestT


def get_allowed_organizations_for_scopes(
    scopes: Collection[str], tree: OrganizationForestT
) -> Set[int]:
    if Scope.superuser in scopes:
        return set(k for k, v in tree.items() if v.data.overordn_orgenhet_id is None)

    allowed = set(map(lambda x: x.removeprefix("organization:"), scopes))
    return set(k for k, v in tree.items() if str(k) in allowed)
