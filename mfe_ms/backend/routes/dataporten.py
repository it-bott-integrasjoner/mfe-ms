import json
import logging
from datetime import datetime, timedelta
from typing import Callable, Optional, List, Tuple, Set, Any, Mapping, Dict
from urllib.parse import urlencode

import requests

from authlib.common.security import generate_token
from authlib.integrations.starlette_client import StarletteOAuth2App
from fastapi import FastAPI, Request
from jose import jwt
from jose.exceptions import ExpiredSignatureError, JWTError
from pydantic import BaseModel, Field
from starlette.authentication import AuthenticationBackend, BaseUser, AuthCredentials
from starlette.datastructures import URLPath, URL
from starlette.requests import HTTPConnection
from starlette.responses import RedirectResponse
from starlette.routing import Router

from mfe_ms.config import (
    MFEConfig,
    AuthorizationType,
    ListsAuthorization,
    EntitlementsAuthorization,
)
from mfe_ms.context import MFEContext
from mfe_ms.backend.auth import Scope

logger = logging.getLogger(__name__)


class User(BaseModel):
    id: str
    name: str
    issued_at: datetime = Field(alias="issuedAt")
    expires_at: datetime = Field(alias="expiresAt")
    scopes: List[str]

    class Config:
        allow_population_by_field_name = True


def is_authorized_by_group(config: MFEConfig, access_token: str) -> bool:
    assert isinstance(config.auth.authorization_config, ListsAuthorization)
    allowed = config.auth.authorization_config.allowed_groups
    if allowed is None:
        logger.info("`allowed_groups` is None, accepting all")
        return True
    if not allowed:
        logger.info("`allowed_groups` is empty, rejecting all")
        return False

    res = requests.get(
        config.auth.dataporten.groups_endpoint,
        headers={"Authorization": f"Bearer {access_token}"},
    )
    res.raise_for_status()

    data = res.json()
    logger.debug("Fetched groups: %s", data)

    groups = set(x["id"] for x in data)
    logger.debug("Got groups: %s", groups)
    accepted_groups = groups & allowed
    if accepted_groups:
        logger.info("Accepting groups: %s", accepted_groups)
    else:
        logger.info("Rejecting groups: %s. Allowed groups are: %s", groups, allowed)

    return bool(accepted_groups)


def is_authorized_by_id(config: MFEConfig, ids: List[str]) -> bool:
    assert isinstance(config.auth.authorization_config, ListsAuthorization)
    if not ids:
        logger.warning("Got empty list of ids, rejecting")
        return False

    allowed = config.auth.authorization_config.allowed_users
    if allowed is None:
        logger.info("`allowed_users` is None, accepting all")
        return True
    res = bool(set(ids) & allowed)
    if res:
        logger.info("Accepting user: %s", ids)
    else:
        logger.info("Rejecting user: %s", ids)
    return res


def is_authorized(config: MFEConfig, ids: List[str], access_token: str) -> bool:
    return is_authorized_by_id(config, ids) or is_authorized_by_group(
        config, access_token
    )


def has_super_user_access(config: MFEConfig, entitlements: Set[str]) -> bool:
    assert isinstance(config.auth.authorization_config, EntitlementsAuthorization)
    if config.auth.authorization_config.allowed_super_user_access is None:
        return False

    for ent_entry in entitlements:
        if ent_entry in config.auth.authorization_config.allowed_super_user_access:
            return True
    return False


def get_organization_scopes_for_user(
    context: MFEContext, entitlements: Set[str]
) -> Set[str]:
    assert isinstance(
        context.config.auth.authorization_config, EntitlementsAuthorization
    )
    entitlement_prefix = (
        context.config.auth.authorization_config.allowed_org_user_entitlement_prefix
    )
    if not entitlement_prefix:
        return set()

    org_ids = set()
    org_by_kortnavn = {
        org.org_kortnavn and org.org_kortnavn.lower(): org
        for org in context.cache.get_organizations()
    }

    for entitlement in entitlements:
        if entitlement.startswith(entitlement_prefix):
            kortnavn = entitlement.removeprefix(entitlement_prefix)
            org = org_by_kortnavn.get(kortnavn)
            if not org:
                err_msg = (
                    f"Invalid config in LDAP/Cerebrum, entitlement: {entitlement} is not valid, "
                    f"Could not find any kortnavn/acronym: {kortnavn} in organization (from SAP)."
                )
                raise ValueError(err_msg)
            org_ids.add(f"organization:{org.id}")

    logger.debug("Scope: Organization ids: %s", org_ids)
    return org_ids


def routes(
    context: MFEContext, router: Router
) -> Tuple[FastAPI, AuthenticationBackend]:
    config = context.config
    dataporten = FastAPI()
    oauth: StarletteOAuth2App = context.oauth.register(
        "dataporten",
        client_id=config.auth.dataporten.client_id,
        client_secret=config.auth.dataporten.client_secret,
        scope="openid",
        server_metadata_url=config.auth.dataporten.well_known_url,
    )

    def decode_token(
        token: str, options: Optional[Mapping[str, Any]] = None
    ) -> Dict[str, Any]:
        return jwt.decode(
            token=token, key=config.auth.secret_key, issuer="mfe-ms", options=options
        )

    def issue_token(
        id_: str,
        name: str,
        id_token: str,
        scope: Optional[List[str]] = None,
    ) -> str:
        now = datetime.utcnow()
        expires_at = now + timedelta(hours=1)
        user = User(
            id=id_,
            name=name,
            expires_at=expires_at,
            issued_at=now,
            scopes=scope or [],
        )

        return jwt.encode(
            dict(
                iss="mfe-ms",
                sub=user.json(),
                exp=expires_at,
                scope=scope,
                id_token=id_token,
            ),
            key=config.auth.secret_key,
            algorithm=config.auth.algorithm,
        )

    class CurrentUserAuthBackend(AuthenticationBackend):
        class DataportenUser(BaseUser, User):
            @property
            def identity(self) -> str:
                return self.id

            @property
            def is_authenticated(self) -> bool:
                return True

            @property
            def display_name(self) -> str:
                return self.name

        async def authenticate(
            self, conn: HTTPConnection
        ) -> Optional[Tuple[AuthCredentials, BaseUser]]:
            token = conn.session.get("token")
            if not token:
                return None
            try:
                token = decode_token(token)
                user = User(**json.loads(token["sub"]))
                return (
                    AuthCredentials(scopes=token["scope"]),
                    CurrentUserAuthBackend.DataportenUser(**user.dict()),
                )
            except ExpiredSignatureError:
                # Keep token to use as id_token_hint
                return None
            except Exception as e:
                logger.debug(e)

            del conn.session["token"]
            return None

    auth_backend = CurrentUserAuthBackend()

    def create_absolute_url(request: Request, path: str) -> URL:
        if config.base_url:
            base_url = URL(config.base_url)
        else:
            base_url = request.base_url
        url_path = URLPath(path=path, protocol="http")
        return url_path.make_absolute_url(base_url)

    def create_url(request: Request, endpoint: Callable[[Request], Any]) -> URL:
        return create_absolute_url(
            request,
            router.url_path_for(
                "dataporten", path=dataporten.url_path_for(endpoint.__name__)
            ),
        )

    @dataporten.get("/callback", include_in_schema=False)
    async def dataporten_callback(request: Request) -> RedirectResponse:
        oauth_token = await oauth.authorize_access_token(request)
        logger.debug("Got OAuth token with scopes: %s", oauth_token.get("scope"))
        userinfo = oauth_token["userinfo"]

        logger.debug("Got user info from dataporten: %s", userinfo)

        scopes: List[str] = []
        if config.auth.authorization_type == AuthorizationType.entitlements:
            extended_userinfo_response = await oauth.request(
                method="GET",
                url=config.auth.dataporten.extended_userinfo_endpoint,
                token=oauth_token,
            )

            extended_userinfo_response.raise_for_status()
            extended_userinfo = extended_userinfo_response.json()
            logger.debug(
                "Got extended user info from dataporten: %s", extended_userinfo
            )

            entitlements = set(extended_userinfo.get("eduPersonEntitlement", []))
            if has_super_user_access(config, entitlements):
                scopes.append(Scope.superuser)
                scopes.append(Scope.authorized)
            elif org_ids := get_organization_scopes_for_user(context, entitlements):
                scopes.extend(org_ids)
                scopes.append(Scope.authorized)

        elif config.auth.authorization_type == AuthorizationType.lists:
            if is_authorized(
                config,
                userinfo["https://n.feide.no/claims/userid_sec"],
                oauth_token.get("access_token"),
            ):
                scopes = [Scope.authorized, Scope.superuser]

        else:
            raise NotImplementedError(
                f"Invalid authorization type: {config.auth.authorization_type}"
            )

        token = issue_token(
            id_=userinfo["sub"],
            name=userinfo["name"],
            id_token=oauth_token["id_token"],
            scope=scopes,
        )

        # Begin user session by logging the user in
        request.session.update({"token": token})

        # Send user back to homepage
        return RedirectResponse(home_url(request))

    def home_url(request: Request) -> URL:
        return create_absolute_url(request, router.url_path_for("frontend", path="/"))

    @dataporten.get("/login", include_in_schema=False)
    async def login(request: Request) -> RedirectResponse:
        callback_uri = create_url(request, dataporten_callback)
        state = generate_token()
        return await oauth.authorize_redirect(  # type: ignore[no-any-return]
            request,
            str(callback_uri),
            state=state,
        )

    @dataporten.get("/logout", include_in_schema=False)
    async def logout(request: Request) -> RedirectResponse:
        metadata = await oauth.load_server_metadata()
        params = {"post_logout_redirect_uri": home_url(request)}
        try:
            token = request.session.pop("token")
            token = decode_token(token)
            params["id_token_hint"] = token["id_token"]
        except (KeyError, JWTError):
            pass
        except Exception as e:
            logger.warning("Exception occurred, ignoring", exc_info=e)
        request.session.clear()
        query = urlencode(params)
        url = f"{metadata['end_session_endpoint']}?{query}"
        logger.debug("Logout redirect url: %s", url)
        return RedirectResponse(url=url)

    return dataporten, auth_backend
