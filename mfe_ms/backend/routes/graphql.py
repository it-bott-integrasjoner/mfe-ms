from __future__ import annotations

import datetime
import logging
import sys
from typing import List, Optional, Tuple, TypedDict, TYPE_CHECKING, TypeAlias, Iterator

import redis
import strawberry
from dfo_sap_client.models import Orgenhet
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import Response
from strawberry.asgi import GraphQL
from strawberry.extensions import SchemaExtension

import mfe_ms.backend.routes.dataporten as dataporten
from mfe_ms.backend.utils import get_allowed_organizations_for_scopes
from mfe_ms.context import MFEContext, get_context
from mfe_ms.tree import build_organization_forest, Tree, OrganizationForestT, clean

logger = logging.getLogger(__name__)


if TYPE_CHECKING:

    class GraphQlContext(TypedDict):
        request: Request
        response: Response
        organization_tree: OrganizationForestT
        mfe_context: MFEContext

    Info: TypeAlias = strawberry.types.Info[GraphQlContext, None]
else:
    from strawberry.types import Info


@strawberry.experimental.pydantic.type(model=dataporten.User)
class User:
    name: strawberry.auto
    expires_at: strawberry.auto
    issued_at: strawberry.auto
    scopes: strawberry.auto


def map_orgenhet_to_organization(
    t: Tree[Orgenhet],
    forest: OrganizationForestT,
) -> Organization:
    org = t.data
    return Organization(
        id=org.id,
        name=org.organisasjonsnavn,
        short_name=org.org_kortnavn,
        parent_id=t.parent.id if t.parent else None,
        # Frontend uses depth to evaluate if node should be expanded
        depth=forest[t.id].depth,
    )


@strawberry.type
class Organization:
    id: int
    name: str
    short_name: str
    parent_id: Optional[int]
    depth: int


@strawberry.type
class CacheInfo:
    last_update: Optional[datetime.datetime]


def routes(context: MFEContext) -> Tuple[Starlette, strawberry.Schema]:
    def resolve_me(info: Info) -> User:
        return info.context["request"].user  # type: ignore[no-any-return]

    def resolve_organizations(info: Info) -> Iterator[Organization]:
        cache = context.cache
        user = info.context["request"].user
        try:
            organizations = cache.get_organizations()
        except (redis.ConnectionError, redis.TimeoutError) as e:
            raise RuntimeError(
                "Fikk ikke kontakt med mellomlager (Redis). Vennligst meld inn feil."
            ) from e

        forest = build_organization_forest(organizations)
        allowed = get_allowed_organizations_for_scopes(set(user.scopes), forest)
        clean_forest = {k: v for k, v in clean(forest=forest, to_keep=allowed).items()}
        info.context["organization_tree"] = clean_forest
        return (map_orgenhet_to_organization(x, forest) for x in clean_forest.values())

    def resolve_cache_info(info: Info) -> CacheInfo:
        return CacheInfo(last_update=context.cache.last_update)

    @strawberry.type
    class Query:
        me: User = strawberry.field(resolver=resolve_me)
        organizations: List[Organization] = strawberry.field(
            resolver=resolve_organizations
        )
        cache: CacheInfo = strawberry.field(resolver=resolve_cache_info)

    class MfeExtension(SchemaExtension):
        def on_request_start(self) -> None:
            self.execution_context.context["mfe_context"] = context

    schema = strawberry.Schema(query=Query, extensions=[MfeExtension])

    app = Starlette()
    app.mount(
        "/",
        GraphQL(
            schema,
            debug=context.config.graphql.debug,
            graphiql=context.config.graphql.enable_gui,
        ),
    )

    return app, schema


def print_schema(context: MFEContext) -> None:
    _, schema = routes(context=context)
    sys.stdout.write(schema.as_str())


if __name__ == "__main__":
    print_schema(context=get_context())
