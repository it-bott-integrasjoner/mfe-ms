"""
Utility functions for use when generating census data file

Mostly put here to avoid cluttering generate_mfe.py
"""


def mg_id_to_text(mg: str) -> str:
    """Convert from mg id to text.

    Returns converted text or original mg id
    """

    mapping = {
        # MG: MG tekst
        "1": "Fast ansatte",
        "2": "Embetsmenn",
        "3": "Åremålsansatte",
        "4": "Midlertidig/engasjerte tjenestemenn",
        "5": "Ekstrahjelp",
        "6": "Vikarer",
        "7": "Tiltak",
        "8": "Eksterne med utbetaling",
        "9": "Eksterne uten utbetaling",
        "B": "Beordret",
        "F": "Avvikende fast ansatte",
        "E": "Avvikende ekstrahjelp",
        "M": "Avvikende midlertidig ansatte",
        "S": "Sjøfolk",
        "T": "Vikar sjøfolk",
        "V": "Avvikende vikarer",
        "X": "Avvikende eksterne med utbetaling",
        "L": "Lokalt ansatte i UD/NORAD",
        "P": "Politikere",
    }
    text = mapping.get(mg)
    return f"{mg} - {text}" if text else mg


def mug_id_to_text(mug: str) -> str:
    """
    Convert from mug id to text

    Returns converted text or original mg id
    """

    mapping = {
        "01": "Hovedlønnstabellen",
        "02": "Hovedlønnstabellen uten opprykk",
        "03": "Lederlønnstabellen",
        "04": "Timelønn",
        "06": "Skift/turnusordning",
        "07": "X-Unge arbeidere",
        "09": "Individuell avlønning",
        "10": "Lærlinger/elever",
        "11": "Rengjøringspersonale",
        "12": "Stipendiater",
        "13": "Postdoktor",
        "14": "Hovedlønnstabellen uten medlemskap i SPK",
        "15": "Timelønn uten medlemskap i SPK",
        "16": "Eksterne prosjekter post 21.1",
        "17": "Interne prosjekter post 21.3",
        "18": "Eksterne prosjekter post 21.1, timelønn",
        "20": "Svalbard hovedlønnstabellen",
        "21": "Svalbard lederlønnstabellen",
        "22": "Svalbard timelønnede",
        "23": "Svalbard honorarer",
        "24": "Svalbard renhold",
        "25": "Statssekr./pol.rådg.",
        "26": "Statsmin./statsråd",
        "27": "Stortingsrepr.",
        "28": "Vararepresentanter",
        "29": "Jan Mayen",
        "30": "Pensjonister på pensjonistvilkår",
        "33": "Bistillinger SPK",
        "34": "Konstituert dommer",
        "35": "Jan Mayen/timelønn",
        "36": "Værobservatører",
        "37": "Andre observatører",
        "38": "Jan Mayen/honorarer",
        "40": "Arbeidsmarkedstiltak",
        "41": "X-Sysselsettingstiltak",
        "42": "X-Attføring",
        "43": "Arbeidsavklaring",
        "44": "Arb.tiltak u/lønn",
        "50": "Eksterne med oppdrag",
        "51": "Styrer/råd/utvalg",
        "52": "Kunstnerlønn",
        "53": "Politikere (dep.)",
        "54": "Oppdrag uten pensjon",
        "55": "Oppdrag med pensjon",
        "56": "Verger",
        "57": "Ektefellebidrag NAV",
        "58": "Helfo utleggstrekk",
        "59": "Eksterne uten oppdrag",
        "60": "Pensjon",
        "62": "Lokalt personale lønnet i Norge",
        "63": "Lokalt personale trygdet og oppgavepliktige i Norge",
        "64": "Lokalt personale oppgavepliktig i Norge",
        "65": "Lokalt personale trygdet i Norge",
        "66": "Lokalt personale",
        "67": "Valgkonsuler",
        "68": "Eksperter NORAD",
        "69": "Observatører UD",
        "70": "Leger",
        "71": "Tolker",
        "72": "Advokater",
        "73": "Revisorer",
        "74": "Andre sakkyndige",
        "75": "Medlem forliksråd",
        "76": "Politireserven",
        "90": "Eksterne ubetalte",
        "91": "Vikar fra vikarbyrå",
        "92": "Sivilarbeidere",
        "93": "Emeritus",
        "94": "Konsulent",
        "95": "Gjesteforsker",
    }
    text = mapping.get(mug)
    return f"{mug} - {text}" if text else mug
